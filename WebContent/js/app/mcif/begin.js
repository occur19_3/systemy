
function BeginSession(id){

    this.id = id;

    this.start = function(){ 

        var msg = {
            "_dmType" : "http://www.invenireaude.com/minicif/party/api#BeginSession"
        };

        
        $.ajax({

            type : "POST",
            url : "/api",
            contentType : "application/json",
            dataType : "json",
            data : JSON.stringify(msg),

            success : function(msg) {

                if (msg._dmType == "IAS.DM.Default#String") {
                    console.log('Server error: ' + msg._value);
                    return;
                }

                console.log(id);
                $(id).show();
            },

            error : function(msg, info) {
                console.log('Error' + info + JSON.stringify(msg));
                return false;
            }
        });

        return false;
    };

};
