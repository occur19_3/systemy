#!/bin/bash

script_dir=$(dirname $(readlink -f ${0}))

if [ -z $1 ] 
then
echo Specify DB Name !!!
exit 1
fi

dbname=${1}
dbfile=/home/team2/bank/Bank/${1}.db

echo
echo DB: ${dbname}
echo
rm -f ${dbfile}

cd ${script_dir}/../sql

for d in ddl dict data
do
find ${dbname}/${d} -name '*.sql' | sort | while read f 
do
echo Processing file: $f
sqlite3 ${dbfile} < ${f}
done
done

echo
echo Done !
echo


