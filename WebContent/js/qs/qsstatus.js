function UsageView(parent) {

	this.canvas = document.createElement('canvas');

	this.width = 200;
	this.height = 15;

	this.margin = 5;

	this.canvas.width = this.width + 3 * this.margin;
	this.canvas.height = this.height + 3 * this.margin;

	parent.appendChild(this.canvas);

	this.value = 0;
	this.valueStep = 10;

	this.background = 'grey';

	ctx = this.canvas.getContext('2d');
	ctx.translate(this.margin, this.margin);

	this.plot = function plot(value) {

		this.value = value;

		ctx = this.canvas.getContext('2d');

		ctx.clearRect(-this.margin, -this.margin, this.width + 2 * this.margin
				+ 1, this.height + 2 * this.margin + 1);

		ctx.shadowBlur = this.margin;
		ctx.shadowColor = "black";

		// ctx.fillStyle=this.style;
		ctx.fillRect(0, 0, this.width, this.height);

		ctx.shadowBlur = 0;
		ctx.shadowColor = 'transparent';

		var gr = ctx.createLinearGradient(0, 0, this.width, 0);
		gr.addColorStop(0, "green");
		gr.addColorStop(0.7, "yellow");
		gr.addColorStop(1.0, "red");

		ctx.fillStyle = gr;
		ctx.fillRect(0, 0, 0.01 * this.value * this.width, this.height);
	};

	this.animate = function animate() {
		this.plot((this.value + this.valueStep - 1) % 100 + 1);
	};

};

function QSStatus(id) {

	this.id = id;
	var o = this;

	function clearTable(tab) {
		var tableRef = document.getElementById(tab);
		while (tableRef.rows.length > 1) {
			tableRef.deleteRow(-1);
		}
	}

	this.fullView = true;

	var sortDirection = 1;

	function compareByName(a, b) {
		return sortDirection == 1 ? (b.name < a.name) : (a.name < b.name);
	}
	function compareByPuts(a, b) {
		return sortDirection * (b.puts.num - a.puts.num);
	}
	function compareByDepth(a, b) {
		return sortDirection * (b.numMsgs - a.numMsgs);
	}
	function compareByGets(a, b) {
		return sortDirection * (b.gets.num - a.gets.num);
	}

	var sortMode = compareByName;

	this.loader = function() {

		$("#infoSpan").text("Loading ...");

		var msg = {
			_dmType : "http://www.invenireaude.com/minicif/party/api#GetParties"
		};

		$.ajax({
			type : "POST",
			url : "/ias/qs/status",
			contentType : "application/json",
			dataType : "json",
			data : JSON.stringify(msg),
			success : function(msg) {

				if (msg._dmType == "IAS/DM/Default#String") {
					console.log('Server error');
					$("#infoSpan").text("Server error!" + msg._value);
					return;
				}

				$("#infoSpan").text("Got response." + msg.generatedOn);

				clearTable();

				msg.queues.sort(sortMode);

				var filter = $('#filterQueues').val();

				for (var i = 0; i < msg.queues.length; i++) {
					if (filter == '' || msg.queues[i].name.match(filter))
						addQueueRow(msg.queues[i]);
				}

				var filter = $('#filterTopics').val();

				for (var i = 0; i < msg.topics.length; i++) {
					if (filter == '' || msg.topics[i].name.match(filter))
						addTopicRow(msg.topics[i]);
				}

				var d = new Date(msg.generatedOn);

				$('#generatedOn').text(d);
			},

			error : function(msg) {
				console.log('Error');
				$("#xmlSpan").text("Error!");
				return false;
			}
		});

		return false;
	};

	var numRows = 0;

	function addQueueRow(q) {

		numRows++;
		if (o.fullView)
			$('table#tblQueues TBODY').append(
					'<tr>' + '<td rowspan="3"><a href="preview.html?queue='
							+ q.name + '">' + q.name + '</a></td>'
							+ '<td rowspan="3">' + q.numMsgs
							+ '</td><td rowspan="3">' + q.highWater
							+ '</td><td rowspan="3">' + q.size + '</td>'
							+ '<td>Puts:</td>' + '<td>' + q.puts.min + '</td>'
							+ '<td>' + q.puts.avg + '</td>' + '<td>'
							+ q.puts.max + '</td>' + '<td>' + q.puts.num
							+ '</td>' + '<td rowspan="3"><div id="usage'
							+ numRows + '"></div></td>' + '</tr><tr>'
							+ '<td>Gets:</td>' + '<td>' + q.gets.min + '</td>'
							+ '<td>' + q.gets.avg + '</td>' + '<td>'
							+ q.gets.max + '</td>' + '<td>' + q.gets.num
							+ '</td>' + '</tr><tr>' + '<td>Waits:</td>'
							+ '<td>' + q.waits.min + '</td>' + '<td>'
							+ q.waits.avg + '</td>' + '<td>' + q.waits.max
							+ '</td>' + '<td>' + q.waits.num + '</td>'
							+ '</tr>');
		else
			$('table#tblQueues TBODY').append(
					'<tr>' + '<td><a href="preview.html?queue=' + q.name
							+ '">' + q.name + '</a></td>' + '<td>' + q.numMsgs
							+ '</td><td>' + q.highWater + '</td><td>' + q.size
							+ '</td>' + '<td><div id="usage' + numRows
							+ '"></div></td>' + '</tr>');

		var c = new UsageView($('#usage' + numRows)[0]);

		c.background = $('.tabPage').css('background-color');

		if (q.size > 0)
			c.plot(100 * q.numMsgs / q.size);
	}

	function addTopicRow(q) {

		numRows++;
		if (o.fullView)
			$('table#tblTopics TBODY').append(
					'<tr>' + '<td rowspan="3"><a href="preview.html?queue='
							+ q.name + '">' + q.name + '</a></td>'
							+ '<td rowspan="3">' + q.numMsgs
							+ '</td><td rowspan="3">' + q.highWater
							+ '</td><td rowspan="3">' + q.size + '</td>'
							+ '<td>Puts:</td>' + '<td>' + q.puts.min + '</td>'
							+ '<td>' + q.puts.avg + '</td>' + '<td>'
							+ q.puts.max + '</td>' + '<td>' + q.puts.num
							+ '</td>' + '</tr><tr>' + '<td>Gets:</td>' + '<td>'
							+ q.gets.min + '</td>' + '<td>' + q.gets.avg
							+ '</td>' + '<td>' + q.gets.max + '</td>' + '<td>'
							+ q.gets.num + '</td>' + '</tr><tr>'
							+ '<td>Waits:</td>' + '<td>' + q.waits.min
							+ '</td>' + '<td>' + q.waits.avg + '</td>' + '<td>'
							+ q.waits.max + '</td>' + '<td>' + q.waits.num
							+ '</td>' + '</tr>');
		else
			$('table#tblTopics TBODY').append(
					'<tr>' + '<td><a href="preview.html?queue=' + q.name + '">'
							+ q.name + '</a></td>' + '<td>' + q.numMsgs
							+ '</td><td>' + q.highWater + '</td><td>' + q.size
							+ '</td>' + '</tr>');
	}

	function clearTable() {

		numRows = 0;
		var header = '<th>Name</th><th>Depth</th><th>Max</th><th>Size</th>';

		if (o.fullView)
			header += '<th>Stats</th><th>Min</th><th>Avg</th><th>Max</th><th>Num</th>';

		header += '<th>Usage</th>'
		$('table#tblQueues THEAD').html('<tr>' + header + '</tr>');
		$('table#tblQueues TBODY').text('');

		header = '<th>Name</th><th>Depth</th><th>Max</th><th>Size</th>';

		if (o.fullView)
			header += '<th>Stats</th><th>Min</th><th>Avg</th><th>Max</th><th>Num</th>';

		$('table#tblTopics THEAD').html('<tr>' + header + '</tr>');
		$('table#tblTopics TBODY').text('');

	}

}
