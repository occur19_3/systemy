IMPORT bank::svc::transaction::getTransactions;

PROGRAM bank::web::bank::getTransactions(
	VAR ctx  AS Context    : "http://www.invenireaude.org/qsystem/workers",
	VAR msg  AS GetTransactions : "http://www.invenireaude.com/bank/transaction/api")
RETURNS GetTransactions : "http://www.invenireaude.com/bank/transaction/api" 						  	
BEGIN
   
   RETURN bank::svc::transaction::getTransactions(ctx,msg);
     
END;

