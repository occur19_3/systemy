/*
 * File: ZZZ-DemoCif/lang/mcif/svc/comm/getCommunicators.y
 * 
 * Copyright (C) 2015, Albert Krzymowski
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

IMPORT  mcif::ds::comm::fetch;

PROGRAM mcif::svc::comm::getCommunicators(
	VAR ctx  AS Context    	       : "http://www.invenireaude.org/qsystem/workers",
	VAR msg  AS GetCommunicators   : "http://www.invenireaude.com/minicif/comm/api")
RETURNS GetCommunicators : "http://www.invenireaude.com/minicif/comm/api" 						  	
BEGIN
 
   msg.communicators=mcif::ds::comm::getCommunicators(msg.selector);
      
   RETURN msg;  
END;

