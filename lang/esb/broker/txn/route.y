/*
 * File: ZZZ-DemoCif/lang/esb/broker/txn/route.y
 * 
 * Copyright (C) 2015, Albert Krzymowski
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
IMPORT std::default;
IMPORT std::qs;
IMPORT esb::ds::fetch;
IMPORT esb::broker::common;
  
  
PROGRAM esb::broker::txn::route(VAR ctx   AS Context  : "http://www.invenireaude.org/qsystem/workers",
			            	    VAR data  AS AnyType)
RETURNS AnyType			            			   	 			 			    	  			 			    	
BEGIN

    VAR system     AS String;
   VAR msgType   AS String;
   
   msgType = TYPE(data);
   system = esb::ds::fetchRouting("TXN",msgType);
      
   IF system == "" THEN BEGIN
     THROW NEW RoutingException : "http://www.examples.org/session" BEGIN
       service = msgType;
     END;
   END;
   
   
   std::send(system,ctx,data);
  
   RETURN std::receive(system,esb::makeFetchContext(ctx));
     
     
END;