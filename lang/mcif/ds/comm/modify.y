/*
 * File: ZZZ-DemoCif/lang/mcif/ds/comm/modify.y
 * 
 * Copyright (C) 2015, Albert Krzymowski
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*******************************************************************************/
/***                  A D D  C O M M U N I C A T O R S                       ***/
/*******************************************************************************/

PROGRAM mcif::ds::comm::addCommunicator(VAR c AS PostalAddress : "http://www.invenireaude.com/minicif/comm" )
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" INSERT INTO CIF_VW_COMMUNICATOR_POSTAL
	         c.pid       => pid,
             c.usage     => usage,
           ? c.label     => label,
   		   ? c.street1   => street1,
   		   ? c.street2   => street2,
		   ? c.city      => city,
		   ? c.zip       => zip,
		   ? c.post      => post,
           ? c.state     => state,
           ? c.countryId => countryId,
           ? c.country   => country
");

PROGRAM mcif::ds::comm::addCommunicator(VAR c AS ElectronicMail : "http://www.invenireaude.com/minicif/comm" )
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" INSERT INTO CIF_VW_COMMUNICATOR_EMAIL
	         c.pid     => pid,
             c.usage   => usage,
           ? c.label   => label,
   		   ? c.email   => email
");

PROGRAM mcif::ds::comm::addCommunicator(VAR c AS Phone : "http://www.invenireaude.com/minicif/comm" )
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" INSERT INTO CIF_VW_COMMUNICATOR_PHONE
	         c.pid     => pid,
             c.usage   => usage,
           ? c.label   => label,
   		   ? c.phone   => phone,
   		   ? c.ext     => ext
");

PROGRAM mcif::ds::comm::addCommunicator(VAR c AS Communicator : "http://www.invenireaude.com/minicif/comm" )
BEGIN

  IF c ISINSTANCE (PostalAddress : "http://www.invenireaude.com/minicif/comm") THEN BEGIN   
    mcif::ds::comm::addCommunicator( (c AS PostalAddress : "http://www.invenireaude.com/minicif/comm") );   
  END ELSE IF c ISINSTANCE (Phone : "http://www.invenireaude.com/minicif/comm") THEN BEGIN
    mcif::ds::comm::addCommunicator( (c AS Phone : "http://www.invenireaude.com/minicif/comm") );
  END ELSE IF c ISINSTANCE (ElectronicMail : "http://www.invenireaude.com/minicif/comm") THEN BEGIN
    mcif::ds::comm::addCommunicator( (c AS ElectronicMail : "http://www.invenireaude.com/minicif/comm") );
  END ELSE 
   BEGIN
    THROW "Unknown type:"+TYPE(c);  
   END;
   
END;

/*******************************************************************************/
/***           D E L E T E     C O M M U N I C A T O R S                     ***/
/*******************************************************************************/

PROGRAM mcif::ds::comm::deleteCommunicator(VAR c AS PostalAddress : "http://www.invenireaude.com/minicif/comm" )
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" DELETE FROM CIF_VW_COMMUNICATOR_POSTAL 
 WHERE
    pid   = c.pid   AND
    usage = c.usage AND
  ? label  = c.label
");

PROGRAM mcif::ds::comm::deleteCommunicator(VAR c AS ElectronicMail : "http://www.invenireaude.com/minicif/comm" )
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" DELETE FROM CIF_VW_COMMUNICATOR_EMAIL
 WHERE
    pid   = c.pid   AND
    usage = c.usage AND
  ? label  = c.label
");

PROGRAM mcif::ds::comm::deleteCommunicator(VAR c AS Phone : "http://www.invenireaude.com/minicif/comm" )
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" DELETE FROM CIF_VW_COMMUNICATOR_PHONE
 WHERE
    pid   = c.pid   AND
    usage = c.usage AND
  ? label  = c.label
");

PROGRAM mcif::ds::comm::deleteCommunicator(VAR c AS Communicator : "http://www.invenireaude.com/minicif/comm" )
BEGIN

  IF c ISINSTANCE (PostalAddress : "http://www.invenireaude.com/minicif/comm") THEN BEGIN   
    mcif::ds::comm::deleteCommunicator( (c AS PostalAddress : "http://www.invenireaude.com/minicif/comm") );   
  END ELSE IF c ISINSTANCE (Phone : "http://www.invenireaude.com/minicif/comm") THEN BEGIN
    mcif::ds::comm::deleteCommunicator( (c AS Phone : "http://www.invenireaude.com/minicif/comm") );
  END ELSE IF c ISINSTANCE (ElectronicMail : "http://www.invenireaude.com/minicif/comm") THEN BEGIN
    mcif::ds::comm::deleteCommunicator( (c AS ElectronicMail : "http://www.invenireaude.com/minicif/comm") );
  END ELSE 
   BEGIN
    THROW "Unknown type:"+TYPE(c);  
   END;
   
END;
