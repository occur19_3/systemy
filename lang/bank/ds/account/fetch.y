/*******************************************************************************/
/***                               A C C O U N T                             ***/
/*******************************************************************************/
 
PROGRAM bank::ds::account::getAccounts(VAR selector AS AccountsSelector : "http://www.invenireaude.com/bank/account/api" )
RETURNS ARRAY OF Account : "http://www.invenireaude.com/bank/account"
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" SELECT ARRAY INTO result
	id => id,
	owner => owner,
   	balance => balance,
    _created => _created
 FROM 
    BANK_ACCOUNT
 WHERE
   ?  id IN selector.aids[*]
");

/*******************************************************************************/
PROGRAM bank::ds::account::getAccount(VAR selector AS AccountSelector : "http://www.invenireaude.com/bank/account/api" )
RETURNS Account : "http://www.invenireaude.com/bank/account"
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" SELECT ONCE
    id  => result.id,
   	owner => result.owner,
   	balance => result.balance,
    _created => result._created
 FROM 
  BANK_ACCOUNT
 WHERE
     id = selector.aid
");

/*******************************************************************************/
PROGRAM bank::ds::account::getClientAccounts(VAR selector AS AccountsByOwnerSelector : "http://www.invenireaude.com/bank/account/api" )
RETURNS ARRAY OF Account : "http://www.invenireaude.com/bank/account"
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" SELECT ARRAY INTO result
    id  => id,
        owner => owner,
        balance => balance,
    _created => _created
 FROM
  BANK_ACCOUNT
 WHERE
     owner = selector.owner
");
/*******************************************************************************/
PROGRAM bank::ds::account::fetch(VAR account  AS Account : "http://www.invenireaude.com/bank/account")
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" SELECT ONCE 
	id  => account.id,
   	owner => account.owner,
    balance => account.balance,
    _created => account._created
 FROM 
  BANK_ACCOUNT
 WHERE
     id = account.id
");
