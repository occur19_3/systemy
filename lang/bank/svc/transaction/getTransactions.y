IMPORT  bank::ds::transaction::fetch;

PROGRAM bank::svc::transaction::getTransactions(VAR ctx  AS Context    : "http://www.invenireaude.org/qsystem/workers",
						  		     VAR msg  AS GetTransactions   : "http://www.invenireaude.com/bank/transaction/api")
RETURNS GetTransactions : "http://www.invenireaude.com/bank/transaction/api" 						  	
BEGIN
 
    msg.transactions=bank::ds::transaction::getTransactions(msg.selector);
   
   RETURN msg;  
END;

