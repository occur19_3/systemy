/*
 * File: ZZZ-DemoCif/lang/mcif/svc/role/getContextRoles.y
 * 
 * Copyright (C) 2015, Albert Krzymowski
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

IMPORT  mcif::ds::role::fetch;
IMPORT  mcif::svc::role::product;

PROGRAM mcif::svc::role::getContextRoles(
	VAR ctx  AS Context    	       : "http://www.invenireaude.org/qsystem/workers",
	VAR msg  AS GetContextRoles    : "http://www.invenireaude.com/minicif/role/api")
RETURNS GetContextRoles : "http://www.invenireaude.com/minicif/role/api" 						  	
BEGIN
 
   msg.roles=mcif::ds::role::getContextRoles(msg.selector);
   
   VAR bFetchChildren AS Boolean;   
   VAR bFetchProducts AS Boolean;

   IF ISSET(msg.selector.fetchChildren) AND msg.selector.fetchChildren == "true" THEN
       bFetchChildren = "true"
    ELSE
       bFetchChildren = "false";

   IF ISSET(msg.selector.fetchProducts) AND msg.selector.fetchProducts == "true" THEN
       bFetchProducts = "true"
    ELSE
       bFetchProducts = "false";
 
   IF bFetchChildren == "true" THEN
   WITH r AS msg.roles DO BEGIN  
    
      r.children=mcif::ds::role::getProductRoles(r.roleId,msg.selector.productRoleTypes);
   
   	  IF bFetchProducts == "true" THEN
      	WITH pr AS r.children DO      
      	  pr.product=mcif::svc::role::getProduct(pr.productId,pr.productType);
      
   END;
   
   RETURN msg;  
END;

