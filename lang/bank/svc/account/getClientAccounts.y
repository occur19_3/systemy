IMPORT  bank::ds::account::fetch;

PROGRAM bank::svc::account::getClientAccounts(VAR ctx  AS Context : "http://www.invenireaude.org/qsystem/workers",
                                              VAR msg  AS GetClientAccounts : "http://www.invenireaude.com/bank/account/api")
RETURNS GetClientAccounts : "http://www.invenireaude.com/bank/account/api"
BEGIN

   msg.accounts=bank::ds::account::getClientAccounts(msg.selector);

   RETURN msg;
END;
