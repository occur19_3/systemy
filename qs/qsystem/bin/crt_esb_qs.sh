#!/bin/bash

script_dir=$(dirname $(readlink -f ${0}))


ias_qs_create -c ${script_dir}/../cfg/QSESB.qs.xml


function qdef(){
ias_qs_change -s DEMOESB -a CreateQueue queueDefinition/name=${1}   queueDefinition/size=100
}

function adef(){
ias_qs_change -s DEMOESB -a CreateLink linkDefinition/name=${1}   linkDefinition/target=${2}
}

qdef ESB.WWW.INQ.IN
qdef ESB.WWW.TXN.IN
qdef ESB.WWW.BANK.INQ.IN
qdef ESB.WWW.BANK.TXN.IN
qdef ESB.WWW.NEWS.INQ.IN
qdef ESB.WWW.NEWS.TXN.IN
qdef WWW.INQ.IN
qdef WWW.TXN.IN
qdef BANK.INQ.IN
qdef BANK.TXN.IN
qdef NEWS.INQ.IN
qdef NEWS.TXN.IN

adef WWW.INQ.OUT ESB.WWW.INQ.IN
adef WWW.TXN.OUT ESB.WWW.TXN.IN

adef ESB.WWW.BANK.INQ.OUT BANK.INQ.IN
adef ESB.WWW.NEWS.INQ.OUT NEWS.INQ.IN

adef ESB.WWW.BANK.TXN.OUT BANK.TXN.IN
adef ESB.WWW.NEWS.TXN.OUT NEWS.TXN.IN

ias_qs_status -s DEMOESB
