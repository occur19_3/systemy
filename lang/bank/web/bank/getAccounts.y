IMPORT bank::svc::account::getAccounts;

PROGRAM bank::web::bank::getAccounts(
	VAR ctx  AS Context    : "http://www.invenireaude.org/qsystem/workers",
	VAR msg  AS GetAccounts : "http://www.invenireaude.com/bank/account/api")
RETURNS GetAccounts : "http://www.invenireaude.com/bank/account/api" 						  	
BEGIN
   
   RETURN bank::svc::account::getAccounts(ctx,msg);
     
END;

