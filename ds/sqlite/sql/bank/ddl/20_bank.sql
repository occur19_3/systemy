
CREATE TABLE BANK_ACCOUNT(
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    owner       INTEGER NOT NULL,
    balance     INTEGER NOT NULL,

    _created    TIMESTAMP NOT NULL DEFAULT (strftime('%Y-%m-%dT%H:%M:%S',current_timestamp)),

    FOREIGN KEY(owner) REFERENCES CIF_PARTY(pid)
);


CREATE TABLE BANK_DICT_TRANSACTION_TYPE(
    type  CHAR(1)     NOT NULL PRIMARY KEY,
    name  VARCHAR(64) NOT NULL
);

CREATE TABLE BANK_LOCATION_DICT (
    prefix  Integer NOT NULL PRIMARY KEY,
    queue_name  TEXT NOT NULL
);

CREATE TABLE BANK_TRANSACTION(
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    date_time   TIMESTAMP NOT NULL DEFAULT (strftime('%Y-%m-%dT%H:%M:%S',current_timestamp)),
    amount      INTEGER NOT NULL,
    account     INTEGER NOT NULL,
    other_account INTEGER NOT NULL,
    typ           CHAR(1) NOT NULL,
    title         TEXT NOT NULL,

    FOREIGN KEY(account) REFERENCES BANK_ACCOUNT(id)
);

CREATE TRIGGER balance_update_add
AFTER INSERT on BANK_TRANSACTION WHEN NEW.typ = '1'
BEGIN
  UPDATE BANK_ACCOUNT SET balance = (balance + NEW.amount)
   WHERE id = NEW.account;
END;

CREATE TRIGGER balance_update_subtract
AFTER INSERT on BANK_TRANSACTION WHEN NEW.typ = '0'
BEGIN
  UPDATE BANK_ACCOUNT SET balance = (balance - NEW.amount)
   WHERE id = NEW.account;
END;
