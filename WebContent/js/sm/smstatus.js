function SMStatus(id) {

	this.id = id;
	var o = this;
	
	o.active=false;
	
	var numRows = 0;

	this.action = function (service, type) {

		$("#infoSpan").text("Action: "+type+", for service: "+service);
		
		var msg = {
			request : {
				actions : [ {
					selector : {
						pairs : [ {
							name : "name",
							value : service
						} ]
					},
					action : type
				} ]
			},
			_dmType : "http://www.invenireaude.org/sm/api#ServiceActionCall"
		};

		$(o.id).val(
				"Action: " + type + " " + service
						+ JSON.stringify(msg, undefined, 1));

		$("#td" + service).html('<img src="icons/Wait.png"/>');

		$
				.ajax({
					type : "POST",
					url : "/ias/sm",
					contentType : "application/json",
					dataType : "json",
					data : JSON.stringify(msg),
					success : function(msg) {

						if (msg._dmType != "http://www.invenireaude.org/sm/api#ServiceActionCall") {

							if (msg._dmType == "IAS/DM/Default#String") {
								console.log('Server error');
								$("#infoSpan").text(
										"Server error!" + msg._value);
								return;
							} else {
								console.log('Server error');
								$("#infoSpan").text("Server error!" + msg);
								return;
							}
						}

						$("#infoSpan").text("Action Completed, wait until the status will be refreshed.");
					},

					error : function(msg) {
						console.log('Error');
						//$("#infoSpan").text("Error!"); 	
						return false;
					}
				});
	}

	this.loader = function() {

		o.active=true;
		
		$("#infoSpan").text("Loading ...");

		clearTable();

		var msg = {
			_dmType : "http://www.invenireaude.org/sm/api#ServiceStatusCall"
		};

		var attrName = $('#attrName').val();

		if (attrName != '') {
			var attrValue = $('#attrValue').val();
			msg.request = {
				selector : {
					pairs : []
				}
			};

			msg.request.selector.pairs.push({
				name : attrName,
				value : attrValue
			});
		}

		$(o.id).val('');

		$("#infoSpan").text("Refreshing ..." + JSON.stringify(msg));

		$.ajax({
					type : "POST",
					url : "/ias/sm",
					contentType : "application/json",
					dataType : "json",
					data : JSON.stringify(msg),
					success : function(msg) {

						if (msg._dmType != "http://www.invenireaude.org/sm/api#ServiceStatusCall") {

							if (msg._dmType == "IAS/DM/Default#String") {
								console.log('Server error');
								$("#infoSpan").text(
										"Server error!" + msg._value);
								return;
							} else {
								console.log('Server error');
								$("#infoSpan").text("Server error!" + msg);
								return;
							}
						}

						$("#infoSpan").text("Got response ... ");

						var filter = $('#filterServices').val();

						for (var i = 0; i < msg.response.services.length; i++) {
							if (filter == ''
									|| msg.response.services[i].name
											.match(filter))
								addServiceRow(msg.response.services[i]);
						}

						$(".svcStart").click(function(){
							o.action($(this).data("sname"),'start');
						});
						
						$(".svcStop").click(function(){
							o.action($(this).data("sname"),'stop');
						});

						o.active=false;
						
						$("#infoSpan").text("Refreshed successfuly ! ");
						
						setTimeout(function() {							
							if(!o.active)
								o.loader();
						}, 5000);

					},

					error : function(msg) {
						console.log('Error');
						return false;
					}
				});

		return false;
	};

	function addServiceRow(s) {

		numRows++;

		var rowText = '<tr>' + '<td>' + s.name + '</td><td>';

		for (var i = 0; i < s.statuses.length; i++) {
			rowText += '<img src="/icons/' + s.statuses[i] + '.png"/>';
		}

		rowText += '</td><td id="td' + s.name + '">';
		rowText += '<img src="/icons/Start.png" data-sname="'+s.name+'" class="svcStart"/>';
		rowText += '<img src="/icons/Stop.png"  data-sname="'+s.name+'" class="svcStop"/>';
		rowText += '</td></tr>';

		$('table#tblServices TBODY').append(rowText);

	}

	function clearTable() {

		numRows = 0;

		var header = '<th>Service</th><th>Instances</th><th>Action</th>';

		$('table#tblServices THEAD').html('<tr>' + header + '</tr>');
		$('table#tblServices TBODY').text('');

	}

};