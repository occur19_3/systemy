function Parties(options){

    this.options = options;
    var o=this;

    function partyHTML(p){

        var html = '';

        if(p._dmType == "http://www.invenireaude.com/minicif/party#Person"){
            html    += '<h4 class="list-group-item-heading">'+p.lastname+', '+p.firstname;
            if (p.maidenname) {
                html += ' (' + p.maidenname + ')';
            }
            html    += '</h4>';
            html    += '<p class="list-group-item-text"> <em>Person('+p.pid+')</em> </p>';
            html    += '<p class="list-group-item-text">';
            html    += (p.sex == 1 ? 'Male' : 'Female');

            html    += ' born on: '+p.birthDate+' ';
            html    += '</p>';
        }
        if(p._dmType == "http://www.invenireaude.com/minicif/party#Organization"){
            html    += '<h4 class="list-group-item-heading">'+p.shortname+'</h4>';
            html    += '<p class="list-group-item-text"><em> Organisation('+p.pid+')</em></p>';
            html    += '<p class="list-group-item-text">'+p.fullname+'</p>';
            html    += '<p class="list-group-item-text"> Establised on: '+p.established+'</p>';
        }


        return html;
    };

    function match(ft,s){
        var len = ft.length;


        for(var i = 0; i < len; i++){
            if(ft[i] != '' && !s.match(ft[i]))
                return false;
        }

        return true;
    }

    if(o.options.filter === undefined){

	o.show = function(){

            var html = '<div class="list-group">';

            var len = o.msg.parties.length;

            for (var i = 0; i < len; i++)
                html += partyHTML(o.msg.parties[i]);
            html += '</div>';

            $(options.target).html(html);
	};

    } else {
	o.show = function(){

	    if(o.msg.parties === undefined)
                return;

            o.msg.parties.sort(function (a,b){
                var aname='';

                if(a._dmType == "http://www.invenireaude.com/minicif/party#Person")
                    aname=a.lastname;

                if(a._dmType == "http://www.invenireaude.com/minicif/party#Organization")
                    aname=a.shortname;

                var bname='';

                if(b._dmType == "http://www.invenireaude.com/minicif/party#Person")
                    bname=b.lastname;

                if(b._dmType == "http://www.invenireaude.com/minicif/party#Organization")
                    bname=b.shortname;

                return bname.toUpperCase() < aname.toUpperCase();

            });

            var html = '<div class="list-group">';


            var len = o.msg.parties.length;

            var filter=$(o.options.filter).val().toUpperCase();
            var ft=filter.split(' ');

            for (var i = 0; i < len; i++){
                if( (o.msg.parties[i]._dmType == "http://www.invenireaude.com/minicif/party#Person" &&
                     match(ft,o.msg.parties[i].firstname.toUpperCase()+' '+o.msg.parties[i].lastname.toUpperCase())) ||
                    (o.msg.parties[i]._dmType == "http://www.invenireaude.com/minicif/party#Organization" &&
                     match(ft,o.msg.parties[i].shortname.toUpperCase()+' '+o.msg.parties[i].fullname.toUpperCase())))
                    html += partyHTML(o.msg.parties[i]);
                else
                    console.log("Rejected: "+JSON.stringify(o.msg.parties[i]));
            }


    	    html += '</div>';

            $(options.target).html(html);
        }
    }

    this.fetch = function(){
        console.log("log: ");
    	var msg = {
            selector : {
//                aids: 1
            },
            _dmType : "http://www.invenireaude.com/minicif/party/api#GetParties"
        };

    	if(options.pids !== undefined && options.pids.length > 0)
            msg.selector.pids = options.pids;

        $.ajax({

            type: "POST",
            url: "/api",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(msg),

            success: function(msg) {

                if(msg._dmType == "IAS.DM.Default#String"){
                    console.log('Server error: '+msg._value);
                    return;
                }

                if(msg.parties === undefined)
                    return;

                o.msg=msg;

                o.show();
            },

            error : function(msg,info){
                console.log('Error'+info+JSON.stringify(msg));
                return false;
            }
        });

 	return false;
    };


    $(document).keypress(function(e) {
        if(e.which == 13)
            o.show();
    });

    $("#search").click(function(e) {
        o.show();
    });
};
