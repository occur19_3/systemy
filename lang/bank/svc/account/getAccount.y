IMPORT  bank::ds::account::fetch;

PROGRAM bank::svc::account::getAccount(VAR ctx  AS Context    : "http://www.invenireaude.org/qsystem/workers",
						  		     VAR msg  AS GetAccount   : "http://www.invenireaude.com/bank/account/api")
RETURNS GetAccount : "http://www.invenireaude.com/bank/account/api"
BEGIN

   msg.account=bank::ds::account::getAccount(msg.selector);

   RETURN msg;
END;
