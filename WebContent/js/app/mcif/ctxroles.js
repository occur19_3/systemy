


function CtxRoles(options){

	this.options = options;

	 function roleHTML(r){
		
		var html = '';
		html    += '<a href="ctxproducts.html?roleId='+r.roleId+'&pid='+options.pid+'" class="list-group-item">';
		html    += r.type;
		html    += '</a>';		
		
		return html;
	};
	
    this.fetch = function(){ 
	   
    	var msg = {
    		selector : {
    			partyId : options.pid
    		},
    		 _dmType : "http://www.invenireaude.com/minicif/role/api#GetContextRoles"
    		};
	    
    	
        $.ajax({ 
                
        		type: "POST", 
                url: "/api",
                contentType: "application/json",                 
				dataType: "json",
			    data: JSON.stringify(msg),
                
			    success: function(msg) {  
			    	
		    	if(msg._dmType == "IAS.DM.Default#String"){
					console.log('Server error: '+msg._value); 
					return;
				}					
		    				    
		    	if(msg.roles === undefined)
		    		return;
		    	
		    	var html = '<div class="list-group">';
		    			    	
		    	var len = msg.roles.length;
		    	
		    	for (var i = 0; i < len; i++) 
		    		html += roleHTML(msg.roles[i]);
		    			    	
		    	html += '</div>';
		    	
				$(options.target).html(html);
			   },
        
				error : function(msg,info){  				
		 			console.log('Error'+info+JSON.stringify(msg));   
					return false;				
		 		}
          });
 
 			return false;
        }; 
   
    
    $(document).keypress(function(e) {
    	if(e.which == 13) 
    		fetch();    	
    });
        
};
