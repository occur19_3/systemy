IMPORT std::qs;
IMPORT bank::ds::account::fetch;
IMPORT bank::ds::dict::fetchBank;

PROGRAM bank::ds::transaction::addTransaction(VAR tr AS BankTransaction : "http://www.invenireaude.com/bank/transaction" )
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" INSERT INTO BANK_TRANSACTION
        tr.amount    => amount,
        tr.account   => account,
        tr.other_account   => other_account,
        tr.typ       => typ,
        tr.title     => title
");

PROGRAM bank::ds::transaction::requestTransaction(
    VAR ctx AS Context            : "http://www.invenireaude.org/qsystem/workers",
    VAR rt  AS RequestTransaction : "http://www.invenireaude.com/bank/transaction/api")
RETURNS RequestTransaction : "http://www.invenireaude.com/bank/transaction/api"
BEGIN
    VAR result AS RequestTransactionResult : "http://www.invenireaude.com/bank/transaction";
    VAR bank_id AS Integer;
    VAR bank_queue AS String;
    bank_id = rt.to_aid / 1000;
    bank_queue = bank::ds::dict::fetchBank(bank_id);

/* sprawdz czy istniejemy */
    VAR from_acc AS Account : "http://www.invenireaude.com/bank/account";
    VAR from_selector AS AccountSelector : "http://www.invenireaude.com/bank/account/api";
    from_selector.aid = rt.from_aid;
    from_acc = bank::ds::account::getAccount(from_selector);
    IF NOT ISSET(from_acc.id) THEN BEGIN 
        result.err = 2;
        result.msg = "Sender account not found";
        rt.result = result;
        RETURN rt;
    END;

/* sprawdz czy mamy hajs */

    IF from_acc.balance < rt.amount THEN BEGIN 
        result.err = 1;
        result.msg = "Not enough money.";
        rt.result = result;
        RETURN rt;
    END;

/* wyslij do odbierajacego banku (moze byc ten sam) */
    result.msg = bank_queue;
    IF bank_queue == "" THEN BEGIN
        result.err = 3;
        result.msg = "Receiver account does not exist.";
        rt.result = result;
    END
    ELSE BEGIN
        std::send(bank_queue, ctx, rt);
    END;

/* juz jest git mozna wstawiac */

    VAR tr AS BankTransaction : "http://www.invenireaude.com/bank/transaction"; 
    tr.amount = rt.amount;
    tr.account = rt.from_aid;
    tr.other_account = rt.to_aid;
    tr.typ = 0;
    tr.title = rt.title;    
    bank::ds::transaction::addTransaction(tr);
    result.err = 0;
    result.msg = "Transfer successful";
    rt.result = result;
    RETURN rt; 
END;


PROGRAM bank::ds::transaction::acceptTransaction(VAR rt AS RequestTransaction : "http://www.invenireaude.com/bank/transaction/api" )
RETURNS RequestTransaction : "http://www.invenireaude.com/bank/transaction/api"
BEGIN
    VAR result AS RequestTransactionResult : "http://www.invenireaude.com/bank/transaction";
    VAR bank_id AS Integer;
    VAR bank_queue AS String;
    bank_id = rt.to_aid / 1000;
    bank_queue = bank::ds::dict::fetchBank(bank_id);

/* sprawdz czy odbiorca istnieje */
    VAR to_acc AS Account : "http://www.invenireaude.com/bank/account";
    VAR to_selector AS AccountSelector : "http://www.invenireaude.com/bank/account/api";
    to_selector.aid = rt.to_aid;
    to_acc = bank::ds::account::getAccount(to_selector);
    IF NOT ISSET(to_acc.id) THEN BEGIN
        result.err = 3;
        result.msg = "Receiver acccount not found.";
        rt.result = result;
        RETURN rt;
    END;

/* juz jest git mozna wstawiac */

    VAR tr AS BankTransaction : "http://www.invenireaude.com/bank/transaction";
    tr.amount = rt.amount;
    tr.account = rt.to_aid;
    tr.other_account = rt.from_aid;
    tr.typ = 1;
    tr.title = rt.title;
    bank::ds::transaction::addTransaction(tr);
    result.err = 0;
    result.msg = "Transfer successful";
    rt.result = result;
    RETURN rt;
END;
