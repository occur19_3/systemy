/*
 * File: ZZZ-DemoCif/lang/mcif/ds/role/modify.y
 * 
 * Copyright (C) 2015, Albert Krzymowski
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*******************************************************************************/
/***                                 R O L E                                 ***/
/*******************************************************************************/

PROGRAM mcif::ds::role::getLastCtxRoleId()
RETURNS Integer
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"(
"ds.bank",
"SELECT ONCE 
  id => result 
 FROM 
  CIF_LAST_CTX_ROLE_ID
");

/*******************************************************************************/
PROGRAM mcif::ds::role::getLastProductRoleId()
RETURNS Integer
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"(
"ds.bank",
"SELECT ONCE 
  id => result 
 FROM 
  CIF_LAST_PROD_ROLE_ID
");

/*******************************************************************************/
PROGRAM mcif::ds::role::addRoleImpl(VAR role AS ContextRole : "http://www.invenireaude.com/minicif/role" )
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"(
"ds.bank",
"INSERT INTO CIF_VW_CTX_ROLE
 role.partyId  => partyId,
 role.type     => type
");
 
/*******************************************************************************/
PROGRAM mcif::ds::role::addRole(VAR role AS ContextRole : "http://www.invenireaude.com/minicif/role" )
BEGIN
  
   mcif::ds::role::addRoleImpl(role);
   role.roleId = mcif::ds::role::getLastCtxRoleId();
   
END;

/*******************************************************************************/
PROGRAM mcif::ds::role::addRoleImpl(VAR role AS ProductRole : "http://www.invenireaude.com/minicif/role" )
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"(
"ds.bank",
"INSERT INTO CIF_VW_PRODUCT_ROLE
 role.ctxRoleId   => ctxRoleId,
 role.type        => type,
 role.productId   => productId,
 role.productType => productType
");
 
/*******************************************************************************/
PROGRAM mcif::ds::role::addRole(VAR role AS ProductRole : "http://www.invenireaude.com/minicif/role" )
BEGIN
  
   mcif::ds::role::addRoleImpl(role);
   role.roleId = mcif::ds::role::getLastProductRoleId();
   
END;