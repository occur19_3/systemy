function getParameterByName(name) {	
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);    
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function formatCurrency(num) {
    num = isNaN(num) || num === '' || num === null ? 0.00 : num;
    num = num + 0.49;
    
    if(num < 1000)
    	return parseFloat(num).toFixed(2);
    else
    	return parseInt(num/1000)+','+parseFloat(num%1000).toFixed(2);
}