/*
 * File: ZZZ-DemoCif/lang/mcif/web/mcif/beginSession.y
 * 
 * Copyright (C) 2015, Albert Krzymowski
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

IMPORT mcif::svc::role::getPartyProducts;
IMPORT session::getCookie;
IMPORT std::cache;
IMPORT mcif::web::mcif::session;

PROGRAM mcif::web::mcif::beginSession(
	VAR ctx  AS Context        : "http://www.invenireaude.org/qsystem/workers",
	VAR msg  AS BeginSession   : "http://www.invenireaude.com/minicif/party/api")
RETURNS BeginSession : "http://www.invenireaude.com/minicif/party/api" 						  	
BEGIN
   
   VAR partyProducts AS GetPartyProducts    : "http://www.invenireaude.com/minicif/role/api";
   
   VAR cookie AS String;  
   cookie = session::getCookie(ctx);
    
   cache::fetchBucket("session",cookie);   	        				
   partyProducts.selector.partyId=(cache::getItem("session","partyId") AS Integer);      
   
   partyProducts.selector.ctxRoleTypes = "WebAppUser";
   partyProducts.selector.productTypes = "WebApp";
   partyProducts.selector.roleTypes    = "User";
   
   partyProducts = mcif::svc::role::getPartyProducts(ctx,partyProducts);
 
   VAR session AS Session : "http://www.invenireaude.com/minicif/shop/session";
   session.ctxRoleId = -1;
    
   WITH p AS partyProducts.products DO BEGIN
   	 IF p.productId == "CIF" THEN
 		WITH c AS p.roles DO BEGIN
 			session.ctxRoleId = c.ctxRoleId;
   		END;
   END; 
   
   IF session.ctxRoleId == -1 THEN
   	RETURN msg;
    
   cache::createOrUpdate("session","mcif.session",session);
      
   cache::releaseBucket("session",cookie);
     
   RETURN msg;     
END;

