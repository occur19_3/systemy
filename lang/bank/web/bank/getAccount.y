IMPORT bank::svc::account::getAccount;

PROGRAM bank::web::bank::getAccount(
	VAR ctx  AS Context    : "http://www.invenireaude.org/qsystem/workers",
	VAR msg  AS GetAccount : "http://www.invenireaude.com/bank/account/api")
RETURNS GetAccount : "http://www.invenireaude.com/bank/account/api" 						  	
BEGIN
   
   RETURN bank::svc::account::getAccount(ctx,msg);
     
END;

