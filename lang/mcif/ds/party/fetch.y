/*
 * File: ZZZ-DemoCif/lang/mcif/ds/party/fetch.y
 * 
 * Copyright (C) 2015, Albert Krzymowski
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*******************************************************************************/
/***                                P A R T Y                                ***/
/*******************************************************************************/
 
PROGRAM mcif::ds::party::getParties(VAR selector AS PartiesSelector : "http://www.invenireaude.com/minicif/party/api" )
RETURNS ARRAY OF Party : "http://www.invenireaude.com/minicif/party"
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" SELECT ARRAY INTO result
  MAP type (
		P   => Person  : 'http://www.invenireaude.com/minicif/party'(
		   ? firstname  => firstname,
   		   ? middlename => middlename,
   		   ? lastname   => lastname,
                   ? maidenname => maidenname,
   		   ? birthDate  => birthDate,
   		   ? sex        => sex
		),
		 O   => Organization : 'http://www.invenireaude.com/minicif/party' (
		   ? shortname   => shortname,
   		   ? fullname    => fullname,
   		   ? established => established
		)
   )
	pid => pid
 FROM 
  CIF_VW_PARTY
 WHERE
   ?  pid IN selector.pids[*]
");
/*******************************************************************************/
PROGRAM mcif::ds::party::getParty(VAR partyId AS PartyId : "http://www.invenireaude.com/minicif/party" )
RETURNS Party : "http://www.invenireaude.com/minicif/party"
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" SELECT ARRAY INTO result
  MAP type (
		P   => Person  : 'http://www.invenireaude.com/minicif/party'(
		   ? firstname  => firstname,
   		   ? middlename => middlename,
   		   ? lastname   => lastname,
                   ? maidenname => maidenname,
   		   ? birthDate  => birthDate,
   		   ? sex        => sex
		),
		 O   => Organization : 'http://www.invenireaude.com/minicif/party' (
		   ? shortname   => shortname,
   		   ? fullname    => fullname,
   		   ? established => established
		)
   )
	pid => pid
 FROM 
  CIF_VW_PARTY
 WHERE
     pid = partyId
");
/*******************************************************************************/
PROGRAM mcif::ds::party::fetch(VAR person  AS Person : "http://www.invenireaude.com/minicif/party")
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" SELECT ONCE 
   ? firstname  => person.firstname,
   ? middlename => person.middlename,
   ? lastname   => person.lastname,
   ? maidenname => person.maidenname,
   ? birthDate  => person.birthDate,
   ? sex        => person.sex
 FROM 
  CIF_PARTY_PERSON
 WHERE
     pid = person.pid
");

PROGRAM mcif::ds::party::fetch(VAR organization  AS Organization : "http://www.invenireaude.com/minicif/party")
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" SELECT ONCE 
   ? shortname   => organization.shortname,
   ? fullname    => organization.fullname,
   ? established => organization.established
 FROM 
  CIF_PARTY_ORGANIZATION
 WHERE
     pid = organization.pid
");
