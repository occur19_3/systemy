
script_dir=$(dirname $(readlink -f ${0}))
cd ${script_dir}/..

sm_stop_service -a

base_dir=~

rm -f ${base_dir}/logs/web/*
rm -f ${base_dir}/logs/esb/*
rm -f ${base_dir}/logs/svc/*

sm_start_service 
sm_dsp_service
