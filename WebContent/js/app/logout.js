
function Logout(options){

    this.options=options;
    var o = this;

    $(o.options.success).hide();
    $(o.options.failure).hide();

    this.logout = function(){ 

        var msg = {
            _dmType : "http://www.invenireaude.com/minicif/web/api#Logout"
        };

        $(o.options.question).hide();

        $.ajax({ 

            type: "POST",
            url: "/fcgi/api",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(msg),

	    success: function(msg) {  
                if(msg._dmType == "IAS.DM.Default#String"){
                    console.log('Server error'+msg._value);
                    return;
                }

                $(o.options.success).show();

            },

            error : function(msg,info){
                $(o.options.failure).show();
                console.log('Error'+info+JSON.stringify(msg));
                return false;
            }
        });

 	return false;
    };

    
};
