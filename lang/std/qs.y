/*
 * File: ZZZ-DemoCif/lang/std/qs.y
 * 
 * Copyright (C) 2015, Albert Krzymowski
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
PROGRAM std::newAttr(VAR name AS String, VAR value AS String)
RETURNS Attribute :  "http://www.invenireaude.org/qsystem/workers"
BEGIN
 VAR a AS Attribute :  "http://www.invenireaude.org/qsystem/workers";
 a.name=name;
 a.value=value;
 RETURN a;
END; 

PROGRAM std::send(VAR name     AS String,
				   VAR ctx      AS Context  : "http://www.invenireaude.org/qsystem/workers",
				   VAR data     AS AnyType)
EXTERNAL "libIASQSystemLib:ias_qs_lang_msgs_proxy:Send"
();


PROGRAM std::receive(VAR name     AS String,
				   	 VAR ctx      AS Context  : "http://www.invenireaude.org/qsystem/workers")
RETURNS AnyType 
EXTERNAL "libIASQSystemLib:ias_qs_lang_msgs_proxy:Receive"
();


PROGRAM std::getNumMsgs(VAR name     AS String)
RETURNS Integer 
EXTERNAL "libIASQSystemLib:ias_qs_lang_msgs_proxy:GetNumMessages"
();
