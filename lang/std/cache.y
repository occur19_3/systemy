/*
 * File: ZZZ-DemoCif/lang/std/cache.y
 * 
 * Copyright (C) 2015, Albert Krzymowski
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
PROGRAM cache::createBucket(VAR cache     AS String,
	                        VAR key       AS String)
EXTERNAL "libIASQSystemLib:ias_qs_lang_cache_proxy:CreateBucket"();


PROGRAM cache::fetchBucket(VAR cache     AS String,
	                       VAR key       AS String)
EXTERNAL "libIASQSystemLib:ias_qs_lang_cache_proxy:FetchBucket"();

PROGRAM cache::deleteBucket(VAR cache     AS String,
	                       VAR key       AS String)
EXTERNAL "libIASQSystemLib:ias_qs_lang_cache_proxy:DeleteBucket"();

PROGRAM cache::releaseBucket(VAR cache    AS String,
	                       VAR key       AS String)
EXTERNAL "libIASQSystemLib:ias_qs_lang_cache_proxy:ReleaseBucket"();


PROGRAM cache::getItem(VAR cache     AS String,
	                   VAR itemKey   AS String)
	                   RETURNS AnyType
EXTERNAL "libIASQSystemLib:ias_qs_lang_cache_proxy:GetItem"();


PROGRAM cache::createOrUpdate(VAR cache     AS String,
	                          VAR itemKey   AS String,
	                          VAR value     AS AnyType)
EXTERNAL "libIASQSystemLib:ias_qs_lang_cache_proxy:CreateOrUpdateItem"();
