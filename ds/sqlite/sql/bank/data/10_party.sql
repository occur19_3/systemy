
--select * from CIF_DICT_COMMUNICATOR;
PRAGMA foreign_keys = ON;

INSERT INTO CIF_PARTY(pid,type) VALUES(1,'O');
INSERT INTO CIF_PARTY_ORGANIZATION VALUES(1,'inaude.com','inventireaude.com','2015-03-01');

INSERT INTO CIF_PARTY(pid,type) VALUES(2,'P');
INSERT INTO CIF_PARTY_PERSON VALUES(2,'Walt',NULL,'Kowalski',NULL,'1931-05-31',1);

INSERT INTO CIF_PARTY(pid,type) VALUES(3,'P');
INSERT INTO CIF_PARTY_PERSON VALUES(3,'John',NULL,'Bikeman','Batman','1970-01-02',1);

INSERT INTO CIF_PARTY(pid,type) VALUES(4,'P');
INSERT INTO CIF_PARTY_PERSON VALUES(4,'Bill',NULL,'Salesman',NULL,'1971-01-02',1);

INSERT INTO CIF_PARTY(pid,type) VALUES(5,'P');
INSERT INTO CIF_PARTY_PERSON VALUES(5,'Alice',NULL,'Smith','NULL','1972-01-02',2);

INSERT INTO CIF_VW_COMMUNICATOR_POSTAL(pid,usage,street1,city,countryId)VALUES(1,'HQ','100 Sunset Blv.','Los Angeles','US');
INSERT INTO CIF_VW_COMMUNICATOR_POSTAL(pid,usage,street1,city,countryId)VALUES(1,'Div','200 Hyde St.','San Francisco','US');
INSERT INTO CIF_VW_COMMUNICATOR_POSTAL(pid,usage,street1,city,countryId)VALUES(2,'Home','300 Wall St.','New York','US');
INSERT INTO CIF_VW_COMMUNICATOR_POSTAL(pid,usage,street1,city,countryId)VALUES(3,'Home','400 Walk St.','London','GB');
INSERT INTO CIF_VW_COMMUNICATOR_POSTAL(pid,usage,street1,city,countryId)VALUES(3,'Default','500 Default St.','London','GB');

INSERT INTO CIF_VW_COMMUNICATOR_PHONE(pid,usage,phone)VALUES(1,'HQ','001 500 500');
INSERT INTO CIF_VW_COMMUNICATOR_PHONE(pid,usage,phone)VALUES(2,'Home','001 600 600');
INSERT INTO CIF_VW_COMMUNICATOR_PHONE(pid,usage,phone)VALUES(3,'Home','001 700 700');
INSERT INTO CIF_VW_COMMUNICATOR_PHONE(pid,usage,phone)VALUES(3,'Default','001 999 999 999');

INSERT INTO CIF_VW_COMMUNICATOR_EMAIL(pid,usage,email) VALUES (1,'HQ','support@invenire.com');
INSERT INTO CIF_VW_COMMUNICATOR_EMAIL(pid,usage,email) VALUES (1,'Work','sales@invenire.com');
INSERT INTO CIF_VW_COMMUNICATOR_EMAIL(pid,usage,email) VALUES (3,'Home','john@first.mywebsite.example.com');
INSERT INTO CIF_VW_COMMUNICATOR_EMAIL(pid,usage,email) VALUES (3,'Default','john@default.mywebsite.example.com');

INSERT INTO CIF_VW_COMMUNICATOR_POSTAL(pid,usage,street1,city,countryId)VALUES(4,'Home','666 Car St.','London','GB');
INSERT INTO CIF_VW_COMMUNICATOR_POSTAL(pid,usage,street1,city,countryId)VALUES(4,'Default','777 Default St.','London','GB');
INSERT INTO CIF_VW_COMMUNICATOR_POSTAL(pid,usage,street1,city,countryId)VALUES(5,'Home','888 Train St.','London','GB');

INSERT INTO CIF_VW_COMMUNICATOR_PHONE(pid,usage,phone)VALUES(4,'Work','001 700 713');
INSERT INTO CIF_VW_COMMUNICATOR_PHONE(pid,usage,phone)VALUES(4,'Default','001 999 999 999');
INSERT INTO CIF_VW_COMMUNICATOR_PHONE(pid,usage,phone)VALUES(5,'Work','001 700 714');
INSERT INTO CIF_VW_COMMUNICATOR_EMAIL(pid,usage,email) VALUES (4,'Work','bs@example.invenireaude.com');
INSERT INTO CIF_VW_COMMUNICATOR_EMAIL(pid,usage,email) VALUES (4,'Default','bill.salesman@example.invenireaude.com');
INSERT INTO CIF_VW_COMMUNICATOR_EMAIL(pid,usage,email) VALUES (5,'Work','alice@example.invenireaude.com');



--SELECT * FROM CIF_VW_PARTY;
--SELECT * FROM CIF_VW_VW_COMMUNICATOR;
