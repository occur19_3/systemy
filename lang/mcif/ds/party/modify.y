/*
 * File: ZZZ-DemoCif/lang/mcif/ds/party/modify.y
 * 
 * Copyright (C) 2015, Albert Krzymowski
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*******************************************************************************/
/***                                P A R T Y                                ***/
/*******************************************************************************/
 
PROGRAM mcif::ds::party::getLastPartyId()
RETURNS Integer
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"(
"ds.bank",
"SELECT ONCE 
  id => result 
 FROM 
  CIF_LAST_PARTY_ID
");

/*******************************************************************************/

PROGRAM mcif::ds::party::addParty(VAR party AS Person : "http://www.invenireaude.com/minicif/party" )
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"(
"ds.bank",
"INSERT INTO CIF_VW_PARTY_PERSON
? party.firstname  => firstname,
? party.middlename => middlename,
? party.lastname   => lastname,
? party.birthDate  => birthDate,
? party.sex        => sex
");


PROGRAM mcif::ds::party::addParty(VAR party AS Organization : "http://www.invenireaude.com/minicif/party" )
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"(
"ds.bank",
"INSERT INTO CIF_VW_PARTY_ORGANIZATION
? party.shortname    => shortname,
? party.fullname     => fullname,
? party.established  => established
");
 
/*******************************************************************************/
PROGRAM mcif::ds::party::addParty(VAR party AS Party : "http://www.invenireaude.com/minicif/party" )
BEGIN
  
 
  IF party ISINSTANCE (Person : "http://www.invenireaude.com/minicif/party") THEN
  BEGIN   
   mcif::ds::party::addParty( (party AS Person : "http://www.invenireaude.com/minicif/party") );   
  END 
  	ELSE IF party ISINSTANCE (Organization : "http://www.invenireaude.com/minicif/party") THEN
  BEGIN
   mcif::ds::party::addParty((party AS Organization : "http://www.invenireaude.com/minicif/party"));
  END 
   ELSE 
   BEGIN
    THROW "Unknown type:"+TYPE(party);  
   END;
   
   party.pid = mcif::ds::party::getLastPartyId();
   
END;
/*******************************************************************************/

PROGRAM mcif::ds::party::updateParty(VAR party AS Person : "http://www.invenireaude.com/minicif/party" )
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"(
"ds.bank",
"UPDATE CIF_PARTY_PERSON SET
	? party.firstname  => firstname,
	? party.middlename => middlename,
	? party.lastname   => lastname,
	? party.birthDate  => birthDate,
	? party.sex        => sex
  WHERE
   pid = party.pid
");


PROGRAM mcif::ds::party::updateParty(VAR party AS Organization : "http://www.invenireaude.com/minicif/party" )
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"(
"ds.bank",
"UPDATE  CIF_PARTY_ORGANIZATION SET
	? party.shortname    => shortname,
	? party.fullname     => fullname,
	? party.established  => established
  WHERE
   pid = party.pid
");
 
/*******************************************************************************/
PROGRAM mcif::ds::party::updateParty(VAR party AS Party : "http://www.invenireaude.com/minicif/party" )
BEGIN
  
 
  IF party ISINSTANCE (Person : "http://www.invenireaude.com/minicif/party") THEN
  BEGIN   
   mcif::ds::party::updateParty( (party AS Person : "http://www.invenireaude.com/minicif/party") );   
  END 
  	ELSE IF party ISINSTANCE (Organization : "http://www.invenireaude.com/minicif/party") THEN
  BEGIN
   mcif::ds::party::updateParty((party AS Organization : "http://www.invenireaude.com/minicif/party"));
  END 
   ELSE 
   BEGIN
    THROW "Unknown type:"+TYPE(party);  
   END;
   
END;

/*******************************************************************************/