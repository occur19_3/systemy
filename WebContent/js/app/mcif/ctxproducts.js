


function CtxProducts(options){

	this.options = options;

	 function productHTML(r){
		
		var html = '';
		html    += '<tr>';
		html    += '<td>' + r.roleId + '</td>';
		html    += '<td>' + r.type + '</td>';
		html    += '<td>' + r.productType + '</td>';
		html    += '<td>' + r.productId + '</td>';
		//TODO product details link
		html    += '</tr>';		
		
		
		return html;
	};
	
    this.fetch = function(){ 
	   
    	var msg = {
    		selector : {
    			roleId : options.ctxRoleId,
    			fetchChildren : true,
    			fetchProducts : true
    		},
    		 _dmType : "http://www.invenireaude.com/minicif/role/api#GetContextRoles"
    		};
	    
    	
        $.ajax({ 
                
        		type: "POST", 
                url: "/api",
                contentType: "application/json",                 
				dataType: "json",
			    data: JSON.stringify(msg),
                
			    success: function(msg) {  
			    	
		    	if(msg._dmType == "IAS.DM.Default#String"){
					console.log('Server error: '+msg._value); 
					return;
				}					
		    				    
		    	if(msg.roles === undefined || 
		    	   msg.roles.length != 1 ||
		    	   msg.roles[0].children === undefined)
		    		return;
		    	
		    	var html = '<table class="table table-striped table-bordered">';
		    			    	
		    	html    += '<thead><tr>';
				html    += '<th> Role Id      </th>';
				html    += '<th> Role Type    </th>';
				html    += '<th> Product Type </th>';
				html    += '<th> Product ID   </th>';				
				html    += '</tr></thead>';		
				html    += '<tbody>';
		    	var len = msg.roles[0].children.length;
		    	
		    	for (var i = 0; i < len; i++) 
		    		html += productHTML(msg.roles[0].children[i]);
		    			    	
		    	html += '</tbody>';
		    	html += '</table>';
		    	
				$(options.target).html(html);
				$(options.ctxNameTarget).html(msg.roles[0].type);
				
			   },
        
				error : function(msg,info){  				
		 			console.log('Error'+info+JSON.stringify(msg));   
					return false;				
		 		}
          });
 
 			return false;
        }; 
   
    
    $(document).keypress(function(e) {
    	if(e.which == 13) 
    		fetch();    	
    });
        
};
