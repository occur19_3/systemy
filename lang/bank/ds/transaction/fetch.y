
/*******************************************************************************/
/***                         T R A N S A C T I O N S                         ***/
/*******************************************************************************/

PROGRAM bank::ds::transaction::getTransactions(VAR selector AS TransactionSelector : "http://www.invenireaude.com/bank/transaction/api")
RETURNS ARRAY OF BankTransaction : "http://www.invenireaude.com/bank/transaction"
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" SELECT ARRAY INTO result
    id => id,
    date_time => date_time,
    amount => amount,
    account => account,
    other_account => other_account,
    typ => typ,
    title => title
 FROM
    BANK_TRANSACTION
 WHERE
    account = selector.aid
");
/*
 ORDER BY
    date_time DESC
*/
/*******************************************************************************/
