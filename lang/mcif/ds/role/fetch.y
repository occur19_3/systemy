/*
 * File: ZZZ-DemoCif/lang/mcif/ds/role/fetch.y
 * 
 * Copyright (C) 2015, Albert Krzymowski
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*******************************************************************************/
/***                               R O L E S                                 ***/
/*******************************************************************************/
 
PROGRAM mcif::ds::role::getContextRoles(VAR selector AS RolesSelector : "http://www.invenireaude.com/minicif/role/api" )
RETURNS ARRAY OF ContextRole : "http://www.invenireaude.com/minicif/role"
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" SELECT ARRAY INTO result
    roleId  => roleId,
    partyId => partyId,
    type    => type
 FROM 
  CIF_VW_CTX_ROLE
 WHERE
     ? partyId = selector.partyId AND
	 ? roleId  = selector.roleId  AND
     ? type IN selector.types[*]
");

PROGRAM mcif::ds::role::getProductRoles(VAR ctxRoleId AS RoleId : "http://www.invenireaude.com/minicif/role",
										VAR productRoleTypes AS ARRAY OF String)
RETURNS ARRAY OF ProductRole : "http://www.invenireaude.com/minicif/role"
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" SELECT ARRAY INTO result
    roleId     => roleId,
    ctxRoleId  => ctxRoleId,
    type       => type,
    productId  => productId,
    productType => productType
 FROM 
  CIF_VW_PRODUCT_ROLE
 WHERE
     ctxRoleId = ctxRoleId AND
   ? type IN productRoleTypes[*]
");

PROGRAM mcif::ds::role::getPartyId(VAR ctxRoleId AS RoleId : "http://www.invenireaude.com/minicif/role")
RETURNS PartyId : "http://www.invenireaude.com/minicif/party"
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" SELECT ONCE    
    partyId => result
 FROM 
  CIF_VW_CTX_ROLE
 WHERE
	 roleId  = ctxRoleId     
");

PROGRAM mcif::ds::role::fetchRoleId(VAR role AS ProductRole : "http://www.invenireaude.com/minicif/role" )
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" SELECT ONCE 
    roleId     => role.roleId
  FROM 
  CIF_VW_PRODUCT_ROLE
 WHERE
    ctxRoleId   = role.ctxRoleId AND
    type        = role.type      AND
    productId   = role.productId AND
    productType = role.productType   
");

/*******************************************************************************/
/***                            P R O D U C T S                              ***/
/*******************************************************************************/
 

PROGRAM mcif::ds::role::getPartyProducts(VAR selector AS PartyProductsSelector : "http://www.invenireaude.com/minicif/role/api" )
RETURNS ARRAY OF ProductRole : "http://www.invenireaude.com/minicif/role"
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" SELECT ARRAY INTO result
	ctxRoleId   => ctxRoleId,
    roleId      => roleId,
    roleType    => type,
    productType => productType,
    productId   => productId
 FROM 
  CIF_VW_PARTY_PRODUCT
 WHERE
    partyId   = selector.partyId        AND
   ? ctxRoleId = selector.ctxRoleId     AND
   ? ctxRoleType  IN selector.ctxRoleTypes[*] AND
   ? roleType     IN selector.roleTypes[*]    AND
   ? productType  IN selector.productTypes[*]
");  
	