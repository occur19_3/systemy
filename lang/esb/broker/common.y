/*
 * File: ZZZ-DemoCif/lang/esb/broker/common.y
 * 
 * Copyright (C) 2015, Albert Krzymowski
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

DEFINE RoutingException : "http://www.examples.org/session" AS 
 EXTENSION OF Exception :  "http://www.invenireaude.org/qsystem/workers"
BEGIN
 service AS String;
END;


PROGRAM esb::makeFetchContext(VAR ctx AS Context  : "http://www.invenireaude.org/qsystem/workers")
RETURNS Context  : "http://www.invenireaude.org/qsystem/workers"
BEGIN
 
   WITH a AS ctx.attributes DO
    IF a.name == "MID" THEN
    RETURN NEW Context  : "http://www.invenireaude.org/qsystem/workers" BEGIN
     attributes = NEW Attribute  : "http://www.invenireaude.org/qsystem/workers" BEGIN
       name="CID";
       value=a.value; 
     END;
   END;
   
END;