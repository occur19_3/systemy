IMPORT  bank::ds::transaction::modify;

PROGRAM bank::svc::transaction::modifyTransactions(
        VAR ctx  AS Context    	         : "http://www.invenireaude.org/qsystem/workers",
        VAR msg  AS ModifyTransactions   : "http://www.invenireaude.com/bank/transaction/api")
RETURNS ModifyTransactions : "http://www.invenireaude.com/bank/transaction/api"
BEGIN

   WITH c AS msg.additions DO
     bank::ds::transaction::addTransaction(c);

   RETURN msg;
END;
