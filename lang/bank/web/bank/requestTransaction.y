IMPORT  bank::svc::transaction::requestTransaction;

PROGRAM bank::web::transaction::requestTransaction(
        VAR ctx  AS Context    	         : "http://www.invenireaude.org/qsystem/workers",
        VAR msg  AS RequestTransaction   : "http://www.invenireaude.com/bank/transaction/api")
RETURNS RequestTransaction : "http://www.invenireaude.com/bank/transaction/api"
BEGIN
    RETURN bank::svc::transaction::requestTransaction(ctx, msg);
END;
