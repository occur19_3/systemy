CREATE TABLE CIF_LAST_PARTY_ID(ID INTEGER);
INSERT INTO CIF_LAST_PARTY_ID VALUES(null);

CREATE TABLE CIF_LAST_CTX_ROLE_ID(ID INTEGER);
INSERT INTO CIF_LAST_CTX_ROLE_ID VALUES(null);

CREATE TABLE CIF_LAST_PROD_ROLE_ID(ID INTEGER);
INSERT INTO CIF_LAST_PROD_ROLE_ID VALUES(null);

-----------------------------------------------------------------------------

 
CREATE TABLE CIF_DICT_PARTY_TYPE(
    type  CHAR(1)     NOT NULL PRIMARY KEY,
    name  VARCHAR(64) NOT NULL
);

CREATE TABLE CIF_DICT_PARTY_SEX(
    type  CHAR(1)     NOT NULL PRIMARY KEY,
    name  VARCHAR(64) NOT NULL
);

-----------------------------------------------------------------------------

CREATE TABLE CIF_PARTY(

    pid        INTEGER NOT NULL PRIMARY KEY,
    type       CHAR(1) NOT NULL,
 
    _created   TIMESTAMP NOT NULL DEFAULT (strftime('%Y-%m-%dT%H:%M:%S',current_timestamp)),
    _updated   TIMESTAMP NOT NULL DEFAULT (strftime('%Y-%m-%dT%H:%M:%S',current_timestamp)),

   FOREIGN KEY(type) REFERENCES CIF_DICT_PARTY_TYPE(type)
);

-----------------------------------------------------------------------------

CREATE TABLE CIF_PARTY_PERSON(

   pid        INTEGER NOT NULL PRIMARY KEY,

   firstname   VARCHAR(128) NOT NULL,
   middlename  VARCHAR(128),
   lastname    VARCHAR(128) NOT NULL,
   maidenname  TEXT,
   birthDate   DATE,
   sex         CHAR(1),

   FOREIGN KEY(pid) REFERENCES CIF_PARTY(pid)
); 

CREATE VIEW CIF_VW_PARTY_PERSON AS 
 SELECT
  p.pid,
  pe.firstname,
  pe.middlename,
  pe.lastname,
  pe.birthDate,
  pe.sex
 FROM
  CIF_PARTY p
 LEFT JOIN CIF_PARTY_PERSON pe ON
  p.pid=pe.pid
 WHERE
  p.type='P';


CREATE TRIGGER CIF_VW_PARTY_PERSON_TII 
 INSTEAD OF INSERT ON CIF_VW_PARTY_PERSON
 FOR EACH ROW
 BEGIN
 
  INSERT INTO CIF_PARTY(type)VALUES('P');
  		
	   
  INSERT INTO CIF_PARTY_PERSON VALUES(
	last_insert_rowid(),
        NEW.firstname,
        NEW.middlename,
        NEW.lastname,
        NEW.birthDate,
        NEW.sex);

 UPDATE CIF_LAST_PARTY_ID SET id = last_insert_rowid();
 END; 

-----------------------------------------------------------------------------

CREATE TABLE CIF_PARTY_ORGANIZATION(

   pid         INTEGER NOT NULL PRIMARY KEY,

   shortname   VARCHAR(128) NOT NULL,
   fullname    VARCHAR(128),
   established DATE,

   FOREIGN KEY(pid) REFERENCES CIF_PARTY(pid)
); 

CREATE VIEW CIF_VW_PARTY_ORGANIZATION AS 
 SELECT
  p.pid,
  pe.shortname,
  pe.fullname,
  pe.established
 FROM
  CIF_PARTY p
 LEFT JOIN CIF_PARTY_ORGANIZATION pe ON
  p.pid=pe.pid
 WHERE
  p.type='O';


CREATE TRIGGER CIF_VW_PARTY_ORGANIZATION_TII 
 INSTEAD OF INSERT ON CIF_VW_PARTY_ORGANIZATION
 FOR EACH ROW
 BEGIN
 
  INSERT INTO CIF_PARTY(pid,type)VALUES(NEW.pid,'O');
  			   
  INSERT INTO CIF_PARTY_ORGANIZATION VALUES(
	NEW.pid,
    	NEW.shortname,
	NEW.fullname,
	NEW.established);

 UPDATE CIF_LAST_PARTY_ID SET id = last_insert_rowid();
 END; 

-----------------------------------------------------------------------------

CREATE VIEW CIF_VW_PARTY AS 
 SELECT *
 FROM CIF_PARTY b
 LEFT OUTER JOIN CIF_PARTY_PERSON p
 ON b.pid = p.pid
 LEFT OUTER JOIN CIF_PARTY_ORGANIZATION o 
 ON b.pid = o.pid;

-----------------------------------------------------------------------------

CREATE TABLE CIF_DICT_COMMUNICATOR(
    typeId  CHAR(1)   NOT NULL PRIMARY KEY,
    type  VARCHAR(64) NOT NULL
);

CREATE TABLE CIF_DICT_USAGE(
    usageId INTEGER   NOT NULL PRIMARY KEY,
    usage   VARCHAR(64) NOT NULL
);

CREATE UNIQUE INDEX CIF_DICT_USAGE_1 ON
	CIF_DICT_USAGE(usage);
	
-----------------------------------------------------------------------------

CREATE TABLE CIF_COMMUNICATOR(

   cid         INTEGER NOT NULL PRIMARY KEY,
   pid         INTEGER NOT NULL,
   typeId      CHAR(1) NOT NULL,
   usageId     INTEGER   NOT NULL,
   label       VARCHAR(32),
    _created   TIMESTAMP NOT NULL DEFAULT (strftime('%Y-%m-%dT%H:%M:%S',current_timestamp)),
    _updated   TIMESTAMP NOT NULL DEFAULT (strftime('%Y-%m-%dT%H:%M:%S',current_timestamp)),

  FOREIGN KEY(pid) REFERENCES CIF_PARTY(pid),
  FOREIGN KEY(typeId) REFERENCES CIF_DICT_COMMUNICATOR(typeId)
  FOREIGN KEY(usageId) REFERENCES CIF_DICT_USAGE(usageId)
); 

CREATE UNIQUE INDEX CIF_COMMUNICATOR_1 ON
	CIF_COMMUNICATOR(pid,typeId,usageId,label);

-----------------------------------------------------------------------------
      
CREATE TABLE CIF_DICT_COMM_POSTAL_COUNTRY(
    countryId  CHAR(2)     NOT NULL PRIMARY KEY,
    country    VARCHAR(64) NOT NULL
);

CREATE UNIQUE INDEX CIF_DICT_COMM_POSTAL_COUNTRY_1 ON
	CIF_DICT_COMM_POSTAL_COUNTRY(country);


CREATE TABLE CIF_COMMUNICATOR_POSTAL(

   cid         INTEGER NOT NULL PRIMARY KEY,
   street1     VARCHAR(128),
   street2     VARCHAR(128),
   zip         VARCHAR(128),
   post        VARCHAR(128),
   city        VARCHAR(128),
   state       VARCHAR(64),
   countryId   CHAR(2) NOT NULL,

   FOREIGN KEY(cid) REFERENCES CIF_COMMUNICATOR(cid) ON DELETE CASCADE,
   FOREIGN KEY(countryId) REFERENCES CIF_DICT_COMM_POSTAL_COUNTRY(countryId) ON DELETE SET NULL
); 

CREATE VIEW CIF_VW_COMMUNICATOR_POSTAL AS
SELECT
   c.cid        ,
   c.pid        ,
   d.usage      ,
   c.label      ,
   cp.street1   ,
   cp.street2   ,
   cp.zip       ,
   cp.post      ,
   cp.city      ,
   cp.state     ,
   cp.countryId ,
   dc.country     
FROM
 CIF_COMMUNICATOR_POSTAL cp
JOIN
 CIF_COMMUNICATOR c
ON cp.cid = c.cid
JOIN
 CIF_DICT_USAGE d
ON d.usageId = c.usageId
JOIN
 CIF_DICT_COMM_POSTAL_COUNTRY dc
ON cp.countryId = dc.countryId;


CREATE TRIGGER CIF_VW_COMMUNICATOR_POSTAL_TII
 INSTEAD OF INSERT ON CIF_VW_COMMUNICATOR_POSTAL 
 FOR EACH ROW
 BEGIN
 
  INSERT INTO CIF_COMMUNICATOR(pid,typeId,usageId,label) 
  	VALUES( NEW.pid,
                'A',
		(SELECT usageId FROM CIF_DICT_USAGE WHERE NEW.usage = usage),                
		NEW.label
	);
  	
  INSERT INTO CIF_COMMUNICATOR_POSTAL
  	VALUES( last_insert_rowid(),
 		NEW.street1     ,
 		NEW.street2     ,
 		NEW.zip         ,
 		NEW.post        ,
 		NEW.city        ,
		NEW.state       ,
    		NEW.countryId
	);

 END; 


CREATE TRIGGER CIF_VW_COMMUNICATOR_POSTAL_TID
 INSTEAD OF DELETE ON CIF_VW_COMMUNICATOR_POSTAL
 FOR EACH ROW
 BEGIN
 
 DELETE FROM CIF_COMMUNICATOR_POSTAL
   WHERE 
	cid = OLD.cid;

  DELETE FROM CIF_COMMUNICATOR
   WHERE 
	cid = OLD.cid;

END; 

-----------------------------------------------------------------------------

CREATE TABLE CIF_COMMUNICATOR_EMAIL(

   cid         INTEGER NOT NULL PRIMARY KEY,
   email       VARCHAR(128),

   FOREIGN KEY(cid) REFERENCES CIF_COMMUNICATOR(cid) ON DELETE CASCADE
); 

CREATE VIEW CIF_VW_COMMUNICATOR_EMAIL AS
SELECT
   c.cid       ,
   c.pid       ,
   d.usage     ,
   c.label     ,
   ce.email       
FROM
 CIF_COMMUNICATOR_EMAIL ce
JOIN
 CIF_COMMUNICATOR c
ON ce.cid = c.cid
JOIN
 CIF_DICT_USAGE d
ON d.usageId = c.usageId;

CREATE TRIGGER CIF_VW_COMMUNICATOR_EMAIL_TII
 INSTEAD OF INSERT ON CIF_VW_COMMUNICATOR_EMAIL
 FOR EACH ROW
 BEGIN
 
  INSERT INTO CIF_COMMUNICATOR(pid,typeId,usageId,label) 
  	VALUES( NEW.pid,
                'E',
		(SELECT usageId FROM CIF_DICT_USAGE WHERE NEW.usage = usage),
                NEW.label
	);
  	
  INSERT INTO CIF_COMMUNICATOR_EMAIL
  	VALUES( last_insert_rowid(),
		NEW.email
	);

 END; 


CREATE TRIGGER CIF_VW_COMMUNICATOR_PMAIL_TID
 INSTEAD OF DELETE ON CIF_VW_COMMUNICATOR_EMAIL
 FOR EACH ROW
 BEGIN
 
  DELETE FROM CIF_COMMUNICATOR_EMAIL
   WHERE 
	cid = OLD.cid;

   DELETE FROM CIF_COMMUNICATOR
   WHERE 
	cid = OLD.cid;

END; 
-----------------------------------------------------------------------------

CREATE TABLE CIF_COMMUNICATOR_PHONE(

   cid         INTEGER NOT NULL PRIMARY KEY,

   phone       VARCHAR(16),
   ext         VARCHAR(16),

   FOREIGN KEY(cid) REFERENCES CIF_COMMUNICATOR(cid) ON DELETE CASCADE
); 
 
CREATE VIEW CIF_VW_COMMUNICATOR_PHONE AS
SELECT
   c.cid       ,
   c.pid       ,
   d.usage     ,
   c.label     ,
   cp.phone    ,
   cp.ext       
FROM
 CIF_COMMUNICATOR_PHONE cp
JOIN
 CIF_COMMUNICATOR c
ON cp.cid = c.cid
JOIN
 CIF_DICT_USAGE d
ON d.usageId = c.usageId;

CREATE TRIGGER CIF_VW_COMMUNICATOR_PHONE_TII
 INSTEAD OF INSERT ON CIF_VW_COMMUNICATOR_PHONE
 FOR EACH ROW
 BEGIN
 
  INSERT INTO CIF_COMMUNICATOR(pid,typeId,usageId,label) 
  	VALUES( NEW.pid,
                'P',
		(SELECT usageId FROM CIF_DICT_USAGE WHERE NEW.usage = usage),
               NEW.label
	);
  	
  INSERT INTO CIF_COMMUNICATOR_PHONE
  	VALUES( last_insert_rowid(),
		NEW.phone,
                NEW.ext
	);

 END; 

CREATE TRIGGER CIF_VW_COMMUNICATOR_PHONE_TID
 INSTEAD OF DELETE ON CIF_VW_COMMUNICATOR_PHONE
 FOR EACH ROW
 BEGIN
 
 DELETE FROM CIF_COMMUNICATOR_PHONE
   WHERE 
	cid = OLD.cid;

  DELETE FROM CIF_COMMUNICATOR
   WHERE 
	cid = OLD.cid;
END; 

-----------------------------------------------------------------------------

CREATE VIEW CIF_VW_COMMUNICATOR AS 
 SELECT 
  *
 FROM CIF_COMMUNICATOR c
JOIN CIF_DICT_USAGE d
 ON  c.usageId = d.usageId
JOIN CIF_DICT_COMMUNICATOR t
 ON  c.typeId = t.typeId
LEFT OUTER JOIN CIF_COMMUNICATOR_POSTAL a
 ON c.cid = a.cid
LEFT OUTER JOIN CIF_DICT_COMM_POSTAL_COUNTRY dc
 ON a.countryId = dc.countryId
LEFT OUTER JOIN CIF_COMMUNICATOR_EMAIL e
 ON c.cid = e.cid 
LEFT OUTER JOIN CIF_COMMUNICATOR_PHONE p 
 ON c.cid = p.cid;
   
CREATE TRIGGER CIF_VW_COMMUNICATOR_TID
 INSTEAD OF DELETE ON CIF_VW_COMMUNICATOR
 FOR EACH ROW
 BEGIN
 
  DELETE FROM CIF_COMMUNICATOR
   WHERE 
	pid = OLD.pid AND 
	usageId = (SELECT usageId FROM CIF_DICT_USAGE WHERE OLD.usage = usage) and
	typeId = OLD.typeId AND
        (label IS NULL OR label = OLD.label);

END; 

-----------------------------------------------------------------------------
    
CREATE TABLE CIF_DICT_CTX_ROLE_TYPE(
    roleTypeId INTEGER      NOT NULL PRIMARY KEY,
    type       VARCHAR(64)  NOT NULL
);

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

CREATE TABLE CIF_DICT_PRODUCT_TYPE(
    productTypeId INTEGER      NOT NULL PRIMARY KEY,
    type          VARCHAR(64)  NOT NULL
);

CREATE UNIQUE INDEX CIF_IDX_DICT_PRODUCT_TYPE_1 ON
	CIF_DICT_PRODUCT_TYPE(type);
	
	
CREATE VIEW CIF_VW_DICT_PRODUCT_TYPE AS
 SELECT
  type
 FROM
  CIF_DICT_PRODUCT_TYPE;

--CREATE TRIGGER CIF_VW_DICT_PRODUCT_TYPE_TII 
-- INSTEAD OF INSERT ON CIF_VW_DICT_PRODUCT_TYPE
--FOR EACH ROW
-- BEGIN
-- 
--  INSERT INTO CIF_DICT_PRODUCT_TYPE(
-- 		productTypeId,
--  	type 
--  	)VALUES((SELECT count(productTypeId)
--  			FROM 
--  			  CIF_DICT_PRODUCT_TYPE) + 1,
--  			NEW.type); 			   
-- END; 
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
  
CREATE TABLE CIF_DICT_PROD_ROLE_TYPE(
    roleTypeId     INTEGER    NOT NULL PRIMARY KEY,
    type           VARCHAR(64)  NOT NULL
);

-----------------------------------------------------------------------------

CREATE TABLE CIF_CONTEXT_ROLE(
   roleId		INTEGER NOT NULL PRIMARY KEY,
   partyId      INTEGER NOT NULL,  
   roleTypeId   INTEGER NOT NULL,
   
   _created    TIMESTAMP NOT NULL DEFAULT (strftime('%Y-%m-%dT%H:%M:%S',current_timestamp)),
    _updated   TIMESTAMP NOT NULL DEFAULT (strftime('%Y-%m-%dT%H:%M:%S',current_timestamp)),
    
   FOREIGN KEY(partyId)        REFERENCES CIF_PARTY(pid),
   FOREIGN KEY(roleTypeId)     REFERENCES CIF_DICT_CTX_ROLE_TYPE(roleTypeId)
); 

CREATE UNIQUE INDEX CIF_IDX_CONTEXT_ROLE_1 ON
	CIF_CONTEXT_ROLE(partyId,roleTypeId);

-----------------------------------------------------------------------------

CREATE TABLE CIF_PRODUCT(
   productId      VARCHAR(128) NOT NULL,   
   productTypeId  INTEGER NOT NULL,
   
    _created    TIMESTAMP NOT NULL DEFAULT (strftime('%Y-%m-%dT%H:%M:%S',current_timestamp)),
    _updated   TIMESTAMP NOT NULL DEFAULT (strftime('%Y-%m-%dT%H:%M:%S',current_timestamp)),
    
   PRIMARY KEY(productId,productTypeId),
   FOREIGN KEY(productTypeId) REFERENCES CIF_DICT_PRODUCT_TYPE(productTypeId)
);

-----------------------------------------------------------------------------

CREATE TABLE CIF_PRODUCT_ROLE(
   roleId	  INTEGER NOT NULL PRIMARY KEY,
   ctxRoleId      INTEGER NOT NULL,
   roleTypeId     INTEGER NOT NULL,  
   productId      VARCHAR(128) NOT NULL,   
   productTypeId  INTEGER NOT NULL,
   
   _created    TIMESTAMP NOT NULL DEFAULT (strftime('%Y-%m-%dT%H:%M:%S',current_timestamp)),
    _updated   TIMESTAMP NOT NULL DEFAULT (strftime('%Y-%m-%dT%H:%M:%S',current_timestamp)),
       
   FOREIGN KEY(ctxRoleId)               REFERENCES CIF_CONTEXT_ROLE(roleId),
   FOREIGN KEY(productId,productTypeId) REFERENCES CIF_PRODUCT(productId,productTypeId)
   
); 

CREATE UNIQUE INDEX CIF_IDX_PRODUCT_ROLE_1 ON
	CIF_PRODUCT_ROLE(ctxRoleId,roleTypeId,productTypeId,productId);

-----------------------------------------------------------------------------

CREATE VIEW CIF_VW_CTX_ROLE AS
  SELECT 
   r.roleId,
   r.partyId,  
   d.type
  FROM
   CIF_CONTEXT_ROLE r
  LEFT OUTER JOIN 
   CIF_DICT_CTX_ROLE_TYPE d 
  ON
   r.roleTypeId = d.roleTypeId;

-----------------------------------------------------------------------------
   
CREATE VIEW CIF_VW_PRODUCT_ROLE AS
  SELECT 
   r.roleId,
   r.ctxRoleId, 
   dr.type type,
   r.productId,      
   dp.type productType
  FROM
   CIF_PRODUCT_ROLE r
  LEFT OUTER JOIN 
   CIF_DICT_PROD_ROLE_TYPE dr 
  ON
   r.roleTypeId = dr.roleTypeId
  LEFT OUTER JOIN 
   CIF_DICT_PRODUCT_TYPE dp
  ON
   r.productTypeId = dp.productTypeId;  

-----------------------------------------------------------------------------
      
CREATE VIEW CIF_VW_PRODUCT AS
  SELECT 
    p.productId,
    d.type
  FROM
   CIF_PRODUCT p
  LEFT OUTER JOIN 
   CIF_DICT_PRODUCT_TYPE d 
  ON
   p.productTypeId = d.productTypeId;

-----------------------------------------------------------------------------

CREATE TRIGGER CIF_VW_CTX_ROLE_TII
 INSTEAD OF INSERT ON CIF_VW_CTX_ROLE
 FOR EACH ROW
 BEGIN
 
  INSERT INTO CIF_CONTEXT_ROLE(roleId,partyId,roleTypeId) 
  	VALUES( NEW.roleId,
  			NEW.partyId,
  			(SELECT
  			   roleTypeId
  			  FROM 
  			    CIF_DICT_CTX_ROLE_TYPE 
  			   WHERE 
  			    NEW.type = type));
  	

 UPDATE CIF_LAST_CTX_ROLE_ID SET id = last_insert_rowid();		   
 END; 

-----------------------------------------------------------------------------
 
 CREATE TRIGGER CIF_VW_PRODUCT_ROLE_TII 
 INSTEAD OF INSERT ON CIF_VW_PRODUCT_ROLE
 FOR EACH ROW
 BEGIN
 
  INSERT INTO CIF_PRODUCT_ROLE(
  	roleId,
  	ctxRoleId,
  	roleTypeId,
  	productId,
  	productTypeId
  	)VALUES( NEW.roleId,
  			 NEW.ctxRoleId,  			
  			(SELECT
  			   roleTypeId
  			  FROM 
  			    CIF_DICT_PROD_ROLE_TYPE 
  			   WHERE 
  			    NEW.type = type),
  			NEW.productId,
  			(SELECT
  			   productTypeId
  			  FROM 
  			    CIF_DICT_PRODUCT_TYPE 
  			   WHERE 
  			    NEW.productType = type));
  	
   UPDATE CIF_LAST_PROD_ROLE_ID SET id = last_insert_rowid();		   		   
 END; 

-----------------------------------------------------------------------------
 
CREATE TRIGGER CIF_VW_PRODUCT_TII 
 INSTEAD OF INSERT ON CIF_VW_PRODUCT
 FOR EACH ROW
 BEGIN
 
  INSERT INTO CIF_PRODUCT(
  	productId,
  	productTypeId 
  	)VALUES(NEW.productId,
  		   (SELECT
  			  productTypeId
  			FROM 
  			  CIF_DICT_PRODUCT_TYPE
  			WHERE 
  			  NEW.type = type));
  			   
 END; 
 
 -----------------------------------------------------------------------------

CREATE VIEW CIF_VW_PARTY_PRODUCT AS
  SELECT 
   cr.roleId  ctxRoleId,
   cr.partyId partyId,  
   cr.type    ctxRoleType,
   pr.roleId      roleId,
   pr.type        roleType,
   pr.productType,
   pr.productId
  FROM 
   CIF_VW_CTX_ROLE cr
 JOIN CIF_VW_PRODUCT_ROLE pr
   ON cr.roleId = pr.ctxRoleId
 ORDER BY cr.partyId,pr.productType,pr.productId;

