/*
 * File: ZZZ-DemoCif/lang/mcif/ds/comm/fetch.y
 * 
 * Copyright (C) 2015, Albert Krzymowski
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*******************************************************************************/
/***                         C O M M U N I C A T O R S                       ***/
/*******************************************************************************/

PROGRAM mcif::ds::comm::getCommunicators(VAR selector AS CommunicatorSelector : "http://www.invenireaude.com/minicif/comm/api" )
RETURNS ARRAY OF Communicator : "http://www.invenireaude.com/minicif/comm"
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" SELECT ARRAY INTO result
  MAP typeId (
     A  => PostalAddress  : 'http://www.invenireaude.com/minicif/comm'(
                   ? street1 => street1,
                   ? street2 => street2,
                   ? city    => city,
                   ? zip     => zip,
                   ? post    => post,
                   ? state   => state,
                     countryId => countryId,
                   ? country => country
     ),
     E  => ElectronicMail : 'http://www.invenireaude.com/minicif/comm' (
                   ? email   => email
     ),
     P  => Phone : 'http://www.invenireaude.com/minicif/comm' (
                   ? phone => phone,
                   ? ext   => ext
     )
  )
  ? label => label,
    usage => usage,
    pid => pid
 FROM 
  CIF_VW_COMMUNICATOR
 WHERE
    pid = selector.pid AND
  ? usage IN selector.usages[*] AND
  ? type  IN selector.types[*]  AND
  ? label IN selector.labels[*]
");

/*******************************************************************************/

PROGRAM mcif::ds::comm::getCommunicator(VAR c AS PostalAddress : "http://www.invenireaude.com/minicif/comm" )
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" SELECT ONCE 
	   ? label   => c.label,
	   ? street1 => c.street1,
	   ? street2 => c.street2,
	   ? city    => c.city,
	   ? zip     => c.zip,
	   ? post    => c.post,
       ? state   => c.state,
         countryId => c.countryId,
       ? country => c.country
       
 FROM 
  CIF_VW_COMMUNICATOR_POSTAL
 WHERE
    pid   = c.pid   AND
  	usage = c.usage AND
  ? label =  c.label
");

/*******************************************************************************/

PROGRAM mcif::ds::comm::getCommunicator(VAR c AS ElectronicMail : "http://www.invenireaude.com/minicif/comm" )
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" SELECT ONCE 
	   ? label   => c.label,
  	   ? email   => c.email
       
 FROM 
  CIF_VW_COMMUNICATOR_EMAIL
 WHERE
    pid   = c.pid   AND
  	usage = c.usage AND
  ? label =  c.label
");

/*******************************************************************************/

PROGRAM mcif::ds::comm::getCommunicator(VAR c AS Phone : "http://www.invenireaude.com/minicif/comm" )
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
" SELECT ONCE 
	   ? label   => c.label,
  	   ? phone  => c.phone,
	   ? ext   => c.ext
 FROM 
  CIF_VW_COMMUNICATOR_PHONE
 WHERE
    pid   = c.pid   AND
  	usage = c.usage AND
  ? label =  c.label
");

/*******************************************************************************/
