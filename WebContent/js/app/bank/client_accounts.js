function ClientAccounts(options){

    this.options = options;
    var o=this;

    function accountHTML(p){
        var html = '';
        if (options.select != undefined && options.select == true) {
            html += '<option value="' + p.id + '">' + p.id + " (Balance: " + p.balance + ")</option>";

        } else {
            html    += '<a href="account.html?aid='+p.id+'" class="list-group-item">';

            //if(p._dmType == "http://www.invenireaude.com/bank/account#Account"){
            html    += '<p class="list-group-item-text"> <em>Account('+p.id+')</em> </p>';
            html    += '<p class="list-group-item-text">';
            html    += 'balance: '+p.balance+' ';;

            html    += 'created on: '+p._created+' ';
            html    += '</p>';
            //}

            html    += '</a>';
        }

        return html;
    };

    function match(ft,s){
        var len = ft.length;


        for(var i = 0; i < len; i++){
            console.log(s);
            if(ft[i] != '' && !s.toString().match(ft[i]))
                return false;
        }

        return true;
    }

    if(o.options.filter === undefined){

        o.show = function(){

            var html = '<div class="list-group">';

            var len = o.msg.accounts.length;

            for (var i = 0; i < len; i++)
                html += accountHTML(o.msg.accounts[i]);
            html += '</div>';

            $(options.target).html(html);
        };

    } else {
        o.show = function(){

            if(o.msg.accounts === undefined)
                return;

            o.msg.accounts.sort(function (a,b){
                var aname='';

                if(a._dmType == "http://www.invenireaude.com/bank/account#Account")
                    aname=a._created;


                var bname='';

                if(b._dmType == "http://www.invenireaude.com/bank/account#Account")
                    bname=b._created;


                return bname.toUpperCase() < aname.toUpperCase();

            });

            var html = '<div class="list-group">';


            var len = o.msg.accounts.length;


            var filter=$(o.options.filter).val().toUpperCase();
            var ft=filter.split(' ');

            for (var i = 0; i < len; i++){
                console.log(o.msg.accounts[i]._dmType);
                console.log(ft);
                if( (//o.msg.accounts[i]._dmType == "http://www.invenireaude.com/bank/account#Account" &&
                     match(ft,o.msg.accounts[i].id)))
                    html += accountHTML(o.msg.accounts[i]);
                else
                    console.log("Rejected: "+JSON.stringify(o.msg.accounts[i]));
            }


            html += '</div>';

            $(options.target).html(html);
        }
    }

    this.fetch = function(){

        var msg = {
            selector : {
                owner: options.client
            },
            _dmType : "http://www.invenireaude.com/bank/account/api#GetClientAccounts"
        };
        console.log(msg);

        $.ajax({

            type: "POST",
            url: "/api",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(msg),

            success: function(data) {

                if(data._dmType == "IAS.DM.Default#String"){
                    console.log('Server error: '+data._value);
                    return;
                }
                console.log(data);

                if(data.accounts === undefined) {
                    $("#transfer-btn").hide();
                    return;
                }

                o.msg=data;

                o.show();
            },

            error : function(msg,info){
                console.log('Error'+info+JSON.stringify(msg));
                return false;
            }
        });

        return false;
    };


    $(document).keypress(function(e) {
        if(e.which == 13)
            o.show();
    });

    $("#search").click(function(e) {
        o.show();
    });
};
