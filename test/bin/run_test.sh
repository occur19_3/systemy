#!/bin/bash

_script_dir=$(dirname $(readlink -f ${0}))

_test_dir=${_script_dir}/../msgs
_work_dir=${_script_dir}/../working
_db_dir=${_script_dir}/../../ds

function help {
     echo
     echo "Dumps table:"
     echo `basename $0` "<parameters>"
     echo "Parameters:"
     echo "  -t <testname>   Testcase name."
	 echo "  -d <dstype>     Database type."
     echo "  -c Copy answer to specification."
     echo
     echo $*
     echo
     exit 1
}

_copy=NO
_reset=YES

while getopts ht:d:cR opt
do
  case $opt in
    h)
      help 0
    ;;
    d)
     _db=$OPTARG
    ;;
    t)
      _test=$OPTARG
    ;;
    c)
      _copy=YES
    ;;
    R)
     _reset=NO    
    ;;
    *)
      help Invalid option.
    ;; 
  esac
done

if [ -z ${_test} ] ; then
help Missing testcase.
fi

_in_dir=${_test_dir}/${_test}
_out_dir=${_work_dir}/${_test}

if [ ! -d ${_in_dir} ] ; then
help ${_in_dir} doesn\'t exists. Missing test case \?
fi

shift `expr $OPTIND - 1`
##################################################################
#. ${_script_dir}/setenv.sh
##################################################################

export IAS_LANG_SRC_DIRS=${_script_dir}/../../lang
#echo ${IAS_LANG_SRC_DIRS}
export IAS_QS_REGISTRY=${_script_dir}/../registry.xml

mkdir -p ${_out_dir}/out
rm -f ${_out_dir}/out/*
rm -f ${_out_dir}/err.txt
rm -f ${_out_dir}/out.txt
rm -f ${_out_dir}/diff.txt

###############################
if [ ${_reset} == YES ]
then
echo Reseting DB ...
${_db_dir}/${_db}/bin/crtdb.sh mcif > ${_out_dir}/crtdb.mcif.log
echo Done with db reset !
fi
###############################

#export IAS_DBG_GLOBAL=+info,-details,+error,+stacktrace,+throw,+data
export IAS_DBG_GLOBAL=+info,-details,+error,+stacktrace,-throw,+data
export IAS_DBG_LANG=+info,-details,+error,+stacktrace,+throw

ias_qs_processor -i dir:${_in_dir}/in?mode=browser \
                -o dir:${_out_dir}/out/'${ID}' \
                 -f ${_in_dir}/spec.qs.json \
                -m processor \
                  > ${_out_dir}/out.txt \
                  2> ${_out_dir}/err.txt

###############################
if [ ${_copy} == YES ]
then
#echo cp ${_out_dir}/out/* ${_in_dir}/out
cp ${_out_dir}/out/* ${_in_dir}/out
fi
###############################

diff -r ${_in_dir}/out ${_out_dir}/out 2> ${_out_dir}/output.diff 1>&2
if [ $? == 0 ]
then 
_result=OK
else 
_result=Failure
fi


printf " %20s %10s - %s \n" ${_test} ${_db} ${_result}


