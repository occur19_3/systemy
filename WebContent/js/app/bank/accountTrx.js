function AccountTrx(options){

    this.options = options;
    var o=this;

    function transactionsHTMLHeader(){

        var html = '';
        html    += '<div class="list-group-item">';

        html    += '<p class="list-group-item-text trx-p">';
        html    += '<span class="trx-span">Title</span>';
        html    += '<span class="trx-span">Amount</span>';
        html    += '<span class="trx-span">Date & time</span>';
        html    += '<span class="trx-span">From</span>';
        html    += '<span class="trx-span">To</span>';
        html    += '</p>';

        html    += '</div>';

        return html;
    };
    function transactionsHTML(p){

        var html = '';
        html    += '<div class="list-group-item">';

        html    += '<p class="list-group-item-text trx-p">';
        html    += '<span class="trx-span">' + p.title + '</span>';
        html    += '<span class="trx-span">' + ((p.typ == 0) ? "-" : "") + p.amount + '</span>';
        html    += '<span class="trx-span">' + p.date_time + '</span>';
        html    += '<span class="trx-span">' + p.account + '</span>';
        html    += '<span class="trx-span">' + p.other_account + '</span>';
        html    += '</p>';

        html    += '</div>';

        return html;
    };

    function match(ft,s){
        var len = ft.length;


        for(var i = 0; i < len; i++){
            console.log(s);
            if(ft[i] != '' && !s.toString().match(ft[i]))
                return false;
        }

        return true;
    }

    if(o.options.filter === undefined){

        o.show = function(){

            var html = '<div class="list-group">';

            var len = o.msg.transactions.length;
            console.log(o.msg.transactions.length);

            html += transactionsHTMLHeader();
            for (var i = 0; i < len; i++)
                html += transactionsHTML(o.msg.transactions[i]);
            html += '</div>';

            $(options.target).html(html);
        };

    } else {
        o.show = function(){

            if(o.msg.transactions === undefined)
                return;

            o.msg.transactions.sort(function (a,b){
                var aname='';

                if(a._dmType == "http://www.invenireaude.com/bank/transactions#Account")
                    aname=a._created;


                var bname='';

                if(b._dmType == "http://www.invenireaude.com/bank/transactions#Account")
                    bname=b._created;


                return bname.toUpperCase() < aname.toUpperCase();

            });

            var html = '<div class="list-group">';


            var len = o.msg.transactionss.length;

            var filter=$(o.options.filter).val().toUpperCase();
            var ft=filter.split(' ');

            for (var i = 0; i < len; i++){
                console.log(o.msg.transactionss[i]._dmType);
                console.log(ft);
                if( (//o.msg.transactionss[i]._dmType == "http://www.invenireaude.com/bank/transactions#Account" &&
                     match(ft,o.msg.transactionss[i].id)))
                    html += transactionsHTML(o.msg.transactionss[i]);
                else
                    console.log("Rejected: "+JSON.stringify(o.msg.transactionss[i]));
            }


            html += '</div>';

            $(options.target).html(html);
        }
    }

    this.fetch = function(){

        var msg = {
            selector : {
                aid : options.aid
            },
            _dmType : "http://www.invenireaude.com/bank/transaction/api#GetTransactions"
        };
        console.log(msg);
        console.log(JSON.stringify(msg));

        $.ajax({

            type: "POST",
            url: "/api",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(msg),

            success: function(data) {
                console.log("success");

                if(data._dmType == "IAS.DM.Default#String"){
                    console.log('Server error: '+data._value);
                    return;
                }
                console.log(data);

                if(data.transactions === undefined)
                    return;

                o.msg=data;

                o.show();
            },

            error : function(msg,info){
                console.log('Error'+info+JSON.stringify(msg));
                return false;
            }
        });

        return false;
    };


    $(document).keypress(function(e) {
        if(e.which == 13)
            o.show();
    });

    $("#search").click(function(e) {
        o.show();
    });
};
