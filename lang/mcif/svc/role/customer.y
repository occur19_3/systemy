/*
 * File: ZZZ-DemoCif/lang/mcif/svc/role/customer.y
 * 
 * Copyright (C) 2015, Albert Krzymowski
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 
PROGRAM mcif::svc::role::getCustomerRoleId(VAR partyId AS PartyId : "http://www.invenireaude.com/minicif/party")
RETURNS RoleId : "http://www.invenireaude.com/minicif/role"
BEGIN

  VAR selector AS RolesSelector : "http://www.invenireaude.com/minicif/role/api"; 
  
  selector.partyId = partyId;
  selector.types="Customer";
   
  VAR ctx AS ARRAY OF ContextRole : "http://www.invenireaude.com/minicif/role";      
  ctx = mcif::ds::role::getContextRoles(selector);
   
  IF SIZEOF(ctx) == 0 THEN
   THROW "Missing customer for party: "+selector.partyId;

  RETURN ctx[0].roleId;
  
END; 