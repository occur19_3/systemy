function QSView(id) {

	this.id = id;
	var o = this;
	this.queue = '_unknown_name_';
	
	this.pageSize = 10;
	this.page = 1;

	this.load = function(queue) {

		if (queue === undefined || queue == 'null' || queue == '')
			return;

		this.queue = queue;
		
		$("#infoSpan").text("Loading ...");

		var msg = {
			selection : {
				destination : queue,
				window : {
					pageOffset : o.pageSize * (o.page - 1),
					pageSize : o.pageSize
				}
			},
			_dmType : "http://www.invenireaude.org/qsystem/service#PreviewMessages"
		};

		$("#infoSpan").text("Loading ..." + JSON.stringify(msg));

		$
				.ajax({
					type : "POST",
					url : "/ias/qs/preview",
					contentType : "application/json",
					dataType : "json",
					data : JSON.stringify(msg),
					success : function(msg) {

						if (msg._dmType != "http://www.invenireaude.org/qsystem/service#PreviewMessages") {

							if (msg._dmType == "IAS/DM/Default#String") {
								console.log('Server error');
								$("#infoSpan").text(
										"Server error!" + msg._value);
								return;
							} else {
								console.log('Server error');
								$("#infoSpan").text("Server error!" + msg);
								return;
							}
						}

						$("#infoSpan").text("Got response. " + o.page + " page.");

						clearTable();

						$("#queueSize").text(msg.status.numMessages);

						if (typeof msg.messages === 'undefined'
								|| msg.messages.length == 0) {

							$('p#pagesList').html('');

							if (msg.status.numMessages > 0) {
								o.page = Math.floor((msg.status.numMessages - 1)
										/ o.pageSize) + 1;
								$("#infoSpan").text(
										'Page out of range. Trying page: '
												+ o.page + '.');
								o.load(queue);
							} else {
								o.page = 1;
							}

							return;

						}

						for (var i = 0; i < msg.messages.length; i++) {
							addMessage(msg.messages[i]);
						}

						$('textarea.rawContent').focus(function() {
							$(this).addClass('rawContentExpanded');
							$('html, body').animate({
								scrollTop : $(this).offset().top - 60
							}, 300);
						});

						$('textarea.rawContent').blur(function() {
							$(this).removeClass('rawContentExpanded');
						});

						var htmlPages = '';

						var numPages = Math.floor((msg.status.numMessages - 1)
								/ o.pageSize) + 1;

						for (var i = 1; i <= numPages; i++) {
							htmlPages += '<li';
							if (i == o.page)
								htmlPages += ' class="active disabled" ';
								htmlPages += '><a href="#" class="page_link">' + i
									+ '</a></li>';

						}

						$('ul#pagesList').html(htmlPages);

						$('ul#pagesList li a').click(function() {
							o.page = parseInt($(this).text());
							o.load(queue);
						});

						$("#infoSpan")
								.append(
										'<div class="alert alert-success alert-dismissable">'
												+ ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> &times;'
												+ '</button>		   Success! Well done its submitted.</div>');

					},

					error : function(msg) {
						console.log('Error');
						clearTable();
						$("#infoSpan")
								.append(
										'<div class="alert alert-danger alert-dismissable">'
												+ ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true"> &times;'
												+ '</button> Operation failed.</div>');

						return false;
					}
				});

		return false;
	};

	var numRows = 0;

	function addMessage(m) {

		numRows++;

		var htmlText = '<tr>';

		htmlText += '<td rowspan="' + (m.context.attributes.length + 1) + '">'
				+ numRows + '</td>';

		for (var i = 0; i < m.context.attributes.length; i++) {

			if (i > 0)
				htmlText += '<tr>';

			var a = m.context.attributes[i];
			htmlText += '<td class="attrName">' + a.name + '</td>';
			htmlText += '<td>' + a.value + '</td>';
			htmlText += '</tr>';
		}

		htmlText += '<tr>';
		htmlText += '<td colspan="2" id="msgContent' + numRows
				+ '"><textarea class="rawContent">' + m.rawContent
				+ '</textarea></td>';
		htmlText += '</tr>';

		$('table#tblMessages TBODY').append(htmlText);
	}

	function clearTable() {

		numRows = 0;
		var header = '<th>No.</th><th colspan="2">Message details</th>';

		$('ul#pagesList').html('');
		$("#queueSize").text('Empty.');
		$('table#tblMessages THEAD').html('<tr>' + header + '</tr>');
		$('table#tblMessages TBODY').text('');

	}
}
