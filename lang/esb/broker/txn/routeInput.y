/*
 * File: ZZZ-DemoCif/lang/esb/broker/txn/routeInput.y
 * 
 * Copyright (C) 2015, Albert Krzymowski
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
IMPORT std::default;
IMPORT std::qs;
IMPORT esb::ds::fetch;
IMPORT esb::broker::common;
  
  
PROGRAM esb::broker::inq::routeInput(VAR ctx   AS Context  : "http://www.invenireaude.org/qsystem/workers",
			            			 VAR data  AS AnyType)    	 			 			    	  			 			    	
BEGIN

   VAR system     AS String;
   VAR msgType   AS String;
   
   msgType = TYPE(data);
   system = esb::ds::fetchRouting("TXN",msgType);
      
   IF system == "" THEN BEGIN
     THROW NEW RoutingException : "http://www.examples.org/session" BEGIN
       service = msgType;
     END;
   END;
   
   WITH a AS ctx.attributes DO
   IF a.name == "REPLY_TO" THEN
     ctx.attributes = NEW Attribute  : "http://www.invenireaude.org/qsystem/workers" BEGIN
      name="ESB_REPLY_TO";
      value=a.value; 
     END;
     
   std::send(system,ctx,data);
END;