IMPORT  bank::ds::account::fetch;

PROGRAM bank::svc::account::getAccounts(VAR ctx  AS Context    : "http://www.invenireaude.org/qsystem/workers",
						  		     VAR msg  AS GetAccounts   : "http://www.invenireaude.com/bank/account/api")
RETURNS GetAccounts : "http://www.invenireaude.com/bank/account/api"
BEGIN

   msg.accounts=bank::ds::account::getAccounts(msg.selector);

   RETURN msg;
END;
