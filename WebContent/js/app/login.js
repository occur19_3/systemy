
function _getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
} 

function Login(options){

    this.options=options;
    var o = this;

    var username = _getCookie("IAS_USERNAME");

    if( username !== undefined || username != ''){
        $(o.options.username).val(username);
        $(o.options.remember).attr('checked', true);
    }

    o.login = function(){ 

        $('.shop-alert').hide();
        $(o.options.progress).show();

        var msg = {
            user : {
                username  : $(o.options.username).val(),
                password  : $(o.options.password).val()
            },
            _dmType : "http://www.invenireaude.com/minicif/web/api#Login"
        };


        $.ajax({ 

            type: "POST",
            url: "/fcgi/api",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(msg),

	    success: function(msg) {  
                if(msg._dmType != "http://www.invenireaude.com/minicif/web/api#Login"){
                    console.log('Server error');
                    $(idErrorText).text("Server error!"+msg._value);
                    return;
                }

                if($(o.options.remember).prop('checked') == true)
                    document.cookie="IAS_USERNAME ="+msg.user.username;

                $('.shop-alert').hide();
                $(o.options.success).show();
                window.location.replace('/html/app/index.html');
            },

            error : function(msg,info){
                console.log('Error'+info+JSON.stringify(msg));
                $('.shop-alert').hide();
                $(o.options.failure).show();
                return false;
            }
        });

 	return false;
    };

    $(o.options.button).click(function(e){
    	o.login();   
    });
    
    $(document).keypress(function(e) {
    	if(e.which == 13) 
            o.login();
    });
    
    return o;    
};
