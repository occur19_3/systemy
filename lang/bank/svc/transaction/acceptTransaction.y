IMPORT  bank::ds::transaction::modify;

PROGRAM bank::svc::transaction::acceptTransaction(
        VAR ctx  AS Context    	         : "http://www.invenireaude.org/qsystem/workers",
        VAR msg  AS RequestTransaction   : "http://www.invenireaude.com/bank/transaction/api")
/* RETURNS RequestTransaction : "http://www.invenireaude.com/bank/transaction/api" */
BEGIN
    VAR dupa AS RequestTransaction : "http://www.invenireaude.com/bank/transaction/api";
    dupa = bank::ds::transaction::acceptTransaction(msg);
    RETURN;
END;
