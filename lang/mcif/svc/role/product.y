/*
 * File: ZZZ-DemoCif/lang/mcif/svc/role/product.y
 * 
 * Copyright (C) 2015, Albert Krzymowski
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

IMPORT  mcif::ds::role::fetch;

PROGRAM mcif::svc::role::getProduct(
	VAR productId   AS ProductId    : "http://www.invenireaude.com/minicif/role",
	VAR type        AS String)
RETURNS Product : "http://www.invenireaude.com/minicif/role" 						  	
BEGIN
 
  
   
   VAR result AS Product : "http://www.invenireaude.com/minicif/role";  
   result.productId=productId; 
   result.type=type;
   RETURN result;
 
 
  
END;

