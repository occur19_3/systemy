/*******************************************************************************/
/***                                P A R T Y                                ***/
/*******************************************************************************/

PROGRAM bank::ds::account::addAccount(VAR account AS Account : "http://www.invenireaude.com/bank/account" )
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"(
"ds.bank",
"INSERT INTO BANK_ACCOUNT
    account.id        => id,
    account.owner     => owner,
    account.balance   => balance,
    account._created  => _created
");

 
/*******************************************************************************/
/*
PROGRAM bank::ds::account::addaccount(VAR account AS account : "http://www.invenireaude.com/bank/account" )
BEGIN
  
 
  IF account ISINSTANCE (Person : "http://www.invenireaude.com/bank/account") THEN
  BEGIN   
   bank::ds::account::addaccount( (account AS Person : "http://www.invenireaude.com/bank/account") );   
  END 
  	ELSE IF account ISINSTANCE (Organization : "http://www.invenireaude.com/bank/account") THEN
  BEGIN
   bank::ds::account::addaccount((account AS Organization : "http://www.invenireaude.com/bank/account"));
  END 
   ELSE 
   BEGIN
    THROW "Unknown type:"+TYPE(account);  
   END;
   
   account.pid = bank::ds::account::getLastaccountId();
   
END;
*/
/*******************************************************************************/

PROGRAM bank::ds::account::updateAccount(VAR account AS Account : "http://www.invenireaude.com/bank/account" )
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"(
"ds.bank",
"UPDATE BANK_ACCOUNT SET
    account.id        => id,
    account.owner     => owner,
    account.balance   => balance,
    account._created  => _created
  WHERE
   id = account.id
");

