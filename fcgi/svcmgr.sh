#!/bin/bash


# FastCGI changes the current directory.

script_dir=$(dirname $(readlink -f ${0}))
cd ${script_dir}/..
exec ias_qs_processor -i 'fcgi://localhost/?mode=input&responderName=output' -o dummy: -l extern sm
