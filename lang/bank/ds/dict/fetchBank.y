PROGRAM bank::ds::dict::fetchBank(VAR bank_id AS Integer)
RETURNS String
EXTERNAL "libIASQSystemLib:ias_qs_lang_db_proxy:WrappedStatement"
(
"ds.bank",
"SELECT ONCE
   queue_name  => result
 FROM BANK_LOCATION_DICT
 WHERE
   prefix = bank_id
");
