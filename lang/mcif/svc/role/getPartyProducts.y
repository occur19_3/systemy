/*
 * File: ZZZ-DemoCif/lang/mcif/svc/role/getPartyProducts.y
 * 
 * Copyright (C) 2015, Albert Krzymowski
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

IMPORT  mcif::ds::role::fetch;
IMPORT  mcif::svc::role::product;

PROGRAM mcif::svc::role::getPartyProducts(
	VAR ctx  AS Context    	       : "http://www.invenireaude.org/qsystem/workers",
	VAR msg  AS GetPartyProducts    : "http://www.invenireaude.com/minicif/role/api")
RETURNS GetPartyProducts : "http://www.invenireaude.com/minicif/role/api" 						  	
BEGIN
 
   VAR tabProductRoles AS ARRAY OF ProductRole : "http://www.invenireaude.com/minicif/role";
   
   tabProductRoles=mcif::ds::role::getPartyProducts(msg.selector);
 
   VAR item AS ProductWithRoles : "http://www.invenireaude.com/minicif/role";
   
   VAR lastId AS ProductId : "http://www.invenireaude.com/minicif/role";
   
   WITH r AS tabProductRoles DO BEGIN
    
    IF lastId <> r.productId THEN BEGIN
    	item = NEW ProductWithRoles : "http://www.invenireaude.com/minicif/role";
    	msg.products=item;
    	lastId = r.productId;
    END;
     
    item.type        = r.productType;
    item.productId   = r.productId;
   
    VAR pr AS ProductRole : "http://www.invenireaude.com/minicif/role";
    pr.roleId  = r.roleId;
    pr.type    = r.type;
    pr.ctxRoleId = r.ctxRoleId;
    item.roles=pr;
    
    
   END;
      
   RETURN msg;  
END;

