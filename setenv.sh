script_dir=$(dirname $(readlink -f ${BASH_SOURCE}))
echo $script_dir

base_dir=$script_dir
echo $base_dir

export PATH=${base_dir}/bin:/home/ias/bin:/opt/mqm/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games
echo $PATH

export IAS_LANG_SRC_DIRS=${base_dir}/lang
echo $IAS_LANG_SRC_DIRS

export IAS_LANG_XSD=${base_dir}/xsd/api.xsd
echo $IAS_LANG_XSD

export IAS_QS_REGISTRY=${base_dir}/registry/registry.xml
echo $IAS_QS_REGISTRY

export IAS_SM_CFGDIR=${base_dir}/sm
echo $IAS_SM_CFGDIR


