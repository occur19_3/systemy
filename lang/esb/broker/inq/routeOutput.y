/*
 * File: ZZZ-DemoCif/lang/esb/broker/inq/routeOutput.y
 * 
 * Copyright (C) 2015, Albert Krzymowski
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
IMPORT std::default;
IMPORT std::qs;
IMPORT esb::ds::fetch;
IMPORT esb::broker::common;
  
  
PROGRAM esb::broker::inq::routeOutput(VAR ctx   AS Context  : "http://www.invenireaude.org/qsystem/workers",
			            			  VAR data  AS AnyType)    	 			 			    	  			 			    	
RETURNS AnyType
BEGIN

  WITH a AS ctx.attributes DO
   IF a.name == "ESB_REPLY_TO" THEN
   	 WITH ra AS ctx.attributes DO
   	 	IF ra.name == "REPLY_TO" THEN
   	 	  ra.value=a.value;

	RETURN data;
	
END;