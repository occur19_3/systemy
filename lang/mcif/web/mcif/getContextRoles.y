/*
 * File: ZZZ-DemoCif/lang/mcif/web/mcif/getContextRoles.y
 * 
 * Copyright (C) 2015, Albert Krzymowski
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
IMPORT mcif::svc::role::getContextRoles;

PROGRAM mcif::web::mcif::getContextRoles(
	VAR ctx  AS Context    : "http://www.invenireaude.org/qsystem/workers",
	VAR msg  AS GetContextRoles : "http://www.invenireaude.com/minicif/role/api")
RETURNS GetContextRoles : "http://www.invenireaude.com/minicif/role/api" 						  	
BEGIN
   
   RETURN mcif::svc::role::getContextRoles(ctx,msg);
     
END;

