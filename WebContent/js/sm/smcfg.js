 function SMCfg(id){
		
	this.id = id;
	var o=this;
	
    this.loader = function(){ 
  
        $("#infoSpan").text("Loading ...");

	    var msg = {
		  	_dmType : "http://www.invenireaude.org/sm/api#ServiceConfigCall"
		  };

		$("#infoSpan").text("Loading ..."+JSON.stringify(msg));
		
            $.ajax({ 
                type: "POST", 
                url: "/ias/sm",
                contentType: "application/json",                 
				dataType: "json",
			    data: JSON.stringify(msg),
                success: function(msg) {  
                
                if(msg._dmType != "http://www.invenireaude.org/sm/api#ServiceConfigCall"){                
			    
			    	if(msg._dmType == "IAS/DM/Default#String"){
						console.log('Server error');   
						$("#infoSpan").text("Server error!"+msg._value); 
						return;
					}else{
						console.log('Server error');   
						$("#infoSpan").text("Server error!"+msg); 
						return;
					}
				}

		
			$("#infoSpan").text("Got response. ");
										
			$(o.id).val(JSON.stringify(msg.response,undefined,1));										
			
		   },
	      
			error : function(msg){  				
		 		console.log('Error');   
				$("#infoSpan").text("Error!"); 	
				return false;				
		 	}
          });
 
 			return false;
        }; 
        
 };
 