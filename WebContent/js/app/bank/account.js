function Account(options){

    this.options = options;
    var o=this;

    function accountHTML(p){

        var html = '';

        //if(p._dmType == "http://www.invenireaude.com/bank/account#Account"){
            html    += '<h4 class="list-group-item-heading">'+p.owner;
            html    += '</h4>';
            html    += '<p class="list-group-item-text"> <em>Account('+p.id+')</em> </p>';
            html    += '<p class="list-group-item-text">';
            html    += 'balance: '+p.balance+' ';;

            html    += 'created on: '+p._created+' ';
            html    += '</p>';
        //}


        return html;
    };

    function match(ft,s){
        var len = ft.length;

        for(var i = 0; i < len; i++){
            console.log(s);
            if(ft[i] != '' && !s.toString().match(ft[i]))
                return false;
        }

        return true;
    }

    console.log(o.options.filter);
    if(o.options.filter === undefined){

        o.show = function(){

            var html = '<div class="list-group">';
            html += accountHTML(o.msg.account);
            console.log(accountHTML(o.msg.account));
            html += '</div>';

            $(options.target).html(html);
        };

    } else {
        o.show = function(){

            if(o.msg.account === undefined)
                return;

            o.msg.account.sort(function (a,b){
                var aname='';

                if(a._dmType == "http://www.invenireaude.com/bank/account#Account")
                    aname=a._created;


                var bname='';

                if(b._dmType == "http://www.invenireaude.com/bank/account#Account")
                    bname=b._created;


                return bname.toUpperCase() < aname.toUpperCase();

            });

            var html = '<div class="list-group">';


            var len = o.msg.account.length;

            var filter=$(o.options.filter).val().toUpperCase();
            var ft=filter.split(' ');

            for (var i = 0; i < len; i++){
                console.log(o.msg.account[i]._dmType);
                console.log(ft);
                if( (//o.msg.account[i]._dmType == "http://www.invenireaude.com/bank/account#Account" &&
                     match(ft,o.msg.account[i].id)))
                    html += accountHTML(o.msg.account[i]);
                else
                    console.log("Rejected: "+JSON.stringify(o.msg.account[i]));
            }


            html += '</div>';

            $(options.target).html(html);
        }
    }

    this.fetch = function(){
        console.log("102");

        var msg = {
            selector : {
            },
            _dmType : "http://www.invenireaude.com/bank/account/api#GetAccount"
        };
        console.log(options.aid);

        if(options.aid !== undefined && options.aid.length > 0) {
            msg.selector.aid = options.aid;
        }
        console.log(JSON.stringify(msg));

        $.ajax({

            type: "POST",
            url: "/api",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(msg),

            success: function(msg) {

                if(msg._dmType == "IAS.DM.Default#String"){
                    console.log('Server error: '+msg._value);
                    return;
                }

                if(msg.account === undefined)
                    return;
                console.log(msg);

                o.msg=msg;

                o.show();
            },

            error : function(msg,info){
                console.log('Error'+info+JSON.stringify(msg));
                return false;
            }
        });

        return false;
    };


    $(document).keypress(function(e) {
        if(e.which == 13)
            o.show();
    });

    $("#search").click(function(e) {
        o.show();
    });
};
