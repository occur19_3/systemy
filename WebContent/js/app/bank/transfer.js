function Transfer(options){

    this.options = options;
    var o=this;

    function transferHTML(p){

        console.log(p);
        var html = '';
        if (p.result[0].err == 0) {

            html += '<div class="alert alert-success">';
            html += p.result[0].msg;
            html += "</div>";
        } else {
            html += '<div class="alert alert-warning">';
            html += p.result[0].msg + " (" + p.result[0].err + ")";
            html += "</div>";

        }

        return html;
    };

    function match(ft,s){
        var len = ft.length;

        for(var i = 0; i < len; i++){
            console.log(s);
            if(ft[i] != '' && !s.toString().match(ft[i]))
                return false;
        }

        return true;
    }

    console.log(o.options.filter);
    if(o.options.filter === undefined){

        o.show = function(){

            var html = '<div class="list-group">';
            html += transferHTML(o.msg);
            console.log(transferHTML(o.msg));
            html += '</div>';

            $(options.target).html(html);
        };

    } else {
        o.show = function(){

            if(o.msg.account === undefined)
                return;

            o.msg.account.sort(function (a,b){
                var aname='';

                if(a._dmType == "http://www.invenireaude.com/bank/account#Transfer")
                    aname=a._created;


                var bname='';

                if(b._dmType == "http://www.invenireaude.com/bank/account#Transfer")
                    bname=b._created;


                return bname.toUpperCase() < aname.toUpperCase();

            });

            var html = '<div class="list-group">';


            var len = o.msg.account.length;

            var filter=$(o.options.filter).val().toUpperCase();
            var ft=filter.split(' ');

            for (var i = 0; i < len; i++){
                console.log(o.msg.account[i]._dmType);
                console.log(ft);
                if( (//o.msg.account[i]._dmType == "http://www.invenireaude.com/bank/account#Transfer" &&
                     match(ft,o.msg.account[i].id)))
                    html += transferHTML(o.msg.account[i]);
                else
                    console.log("Rejected: "+JSON.stringify(o.msg.account[i]));
            }


            html += '</div>';

            $(options.target).html(html);
        }
    }

    this.fetch = function(){

        var msg = {
            _dmType : "http://www.invenireaude.com/bank/transaction/api#RequestTransaction"
        };
        console.log(options);

        if(options.from_aid !== undefined) {
            msg.from_aid = options.from_aid;
        }
        if(options.to_aid !== undefined) {
            msg.to_aid = options.to_aid;
        }
        if(options.amount !== undefined) {
            msg.amount = options.amount;
        }
        if(options.title !== undefined && options.title.length > 0) {
            msg.title = options.title;
        }
        console.log(JSON.stringify(msg));

        $.ajax({

            type: "POST",
            url: "/apitxn",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(msg),

            success: function(msg) {
                console.log(msg);

                if(msg._dmType == "IAS.DM.Default#String"){
                    console.log('Server error: '+msg._value);
                    return;
                }

                if(msg.result === undefined)
                    return;
                console.log(msg);

                o.msg=msg;

                o.show();
            },

            error : function(msg,info){
                console.log('Error'+info+JSON.stringify(msg));
                return false;
            }
        });

        return false;
    };


    $(document).keypress(function(e) {
        if(e.which == 13)
            o.show();
    });

    $("#search").click(function(e) {
        o.show();
    });
};
